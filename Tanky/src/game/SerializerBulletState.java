package game;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializer;
import com.jme3.network.serializing.serializers.Vector3Serializer;

public class SerializerBulletState extends Serializer {

	@SuppressWarnings("unchecked")
	public BulletState readObject(ByteBuffer data, @SuppressWarnings("rawtypes") Class state) throws IOException {
		int id = data.getInt();
		Vector3f vector = (Vector3f) Vector3Serializer.readClassAndObject(data);
		float w = data.getFloat();
		float x = data.getFloat();
		float y = data.getFloat();
		float z = data.getFloat();
		Quaternion q = new Quaternion(x, y, z, w);
		int b = data.getInt();
		float dmg = data.getFloat();
		BulletState bulletState = new BulletState(id, q, vector, b, dmg);
		return bulletState;
	}

	@Override
	public void writeObject(ByteBuffer buffert, Object state) throws IOException {
		//buffert.clear();
		//System.out.println(buffert.remaining());
		buffert.putInt(((BulletState)state).id);
		Vector3Serializer.writeClassAndObject(buffert, ((BulletState)state).pos);
		buffert.putFloat(((BulletState)state).rot.getW());
		buffert.putFloat(((BulletState)state).rot.getX());
		buffert.putFloat(((BulletState)state).rot.getY());
		buffert.putFloat(((BulletState)state).rot.getZ());
		buffert.putInt(((BulletState)state).bounces);
		buffert.putFloat(((BulletState)state).dmg);
		
	}

}
