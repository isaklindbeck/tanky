package game;

import java.util.ArrayList;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessageWorldState extends AbstractMessage{
	public ArrayList<PlayerState> playerStates;
	public ArrayList<BulletState> bulletStates;
	public int syncId;
	public boolean real = true;
	
	public MessageWorldState(){
		
	}
	
	public MessageWorldState(ArrayList<PlayerState> pstate, ArrayList<BulletState> bstate, int id){
		this.playerStates = pstate;
		this.bulletStates = bstate;
		this.syncId = id;
	}
}
