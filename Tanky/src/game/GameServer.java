package game;

import java.io.IOException;
import java.util.ArrayList;

import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.network.serializing.Serializer;


public class GameServer implements ConnectionListener, MessageListener<HostedConnection> {
	private Server server;
	private String[][] map;
	private ArrayList<String> playerList;
	private int syncId;
	private int score;
	private int nbrPlayers;
	private ArrayList<MessagePlayerControll> ctrlList;
	
	
	public GameServer(int port, String gameName, String[][] map, String playerName, int score, int nbrPlayers) {
		super();
		try {
			server = Network.createServer(port);
			server.start();
			server.addConnectionListener(this);
			server.addMessageListener(this, MessagePlayerState.class);
			server.addMessageListener(this, MessageInitiziatePlayer.class);
			server.addMessageListener(this, MessagePlayerControll.class);
			server.addMessageListener(this, MessageDisconnectPlayer.class);
			
			initzializeClasses();
			System.out.println("Server connected: " + server.isRunning());
			
		} catch (IOException e) {
			System.out.println("Failed to create server.");
			e.printStackTrace();
		}
		syncId = 0;
		ctrlList = new ArrayList<MessagePlayerControll>();
		playerList = new ArrayList<String>();
		//this.playerName = playerName;
		if (playerName != null) playerList.add(playerName);
		this.map = map;
		this.score = score;
		this.nbrPlayers = nbrPlayers;
	}
	
	public static void initzializeClasses(){
		Serializer.registerClass(PlayerState.class, new SerializerPlayerState());
		Serializer.registerClass(BulletState.class, new SerializerBulletState());
		Serializer.registerClass(MessagePlayerState.class);
		Serializer.registerClass(MessageWorldState.class);
		Serializer.registerClass(MessageWorldMap.class);
		Serializer.registerClass(MessageInitiziatePlayer.class);
		Serializer.registerClass(MessagePlayerControll.class);
		Serializer.registerClass(MessageDisconnectPlayer.class);
	}

	@Override
	public void connectionAdded(Server arg0, HostedConnection con) {		
		System.out.println("Client connected");
		
		
		con.send(new MessageWorldMap(map, score));
		//con.send(new TankyMessageInitiziatePlayer(playerName));
		for (int i = 0; i < playerList.size(); i++) {
			con.send(new MessageInitiziatePlayer(playerList.get(i)));
		}
		
	}

	@Override
	public void connectionRemoved(Server s, HostedConnection con) {
		String player = (String) con.getAttribute("player");
		int index = playerList.indexOf(player);
		if (index >= 0) {
			playerList.remove(index);
		}
		
		System.out.println("Conn remoevd, player: " + player);
	}

	@Override
	public void messageReceived(HostedConnection arg0, Message arg1) {
		if (arg1 instanceof MessagePlayerControll) {
			MessagePlayerControll ctrl = ((MessagePlayerControll) arg1);
			for (int i = 0; i < ctrlList.size(); i++) {
				if (ctrlList.get(i).playerName.equals(ctrl.playerName) && (ctrlList.get(i).syncID < ctrl.syncID || (ctrlList.get(i).syncID - 50000) > ctrl.syncID)) {
					ctrlList.get(i).update(ctrl);
				}
			}
		}
	
		if (arg1 instanceof MessageInitiziatePlayer) {
			String name = ((MessageInitiziatePlayer)arg1).name;
			if (!playerList.contains(name) && playerList.size() < nbrPlayers) {
				System.out.println("Initziating player: " + name);
				System.out.println(nbrPlayers + "    " + playerList.size());
				playerList.add(name);
				MessagePlayerControll ctrl = new MessagePlayerControll(
						name, 0, false, false, false, false, false, 0);
				ctrlList.add(ctrl);
				server.broadcast(new MessageInitiziatePlayer(name));
				arg0.setAttribute("player", name);
			} else if (playerList.size() >= nbrPlayers) {
				System.out.println("The game is full");
				arg0.close("The game is full");
			} else {
				System.out.println("Players name already in use");
				arg0.close("Player name already in use");
			}
		}
		
		if (arg1 instanceof MessageDisconnectPlayer) {
			String name = ((MessageDisconnectPlayer)arg1).name;
			for (int i = 0; i < playerList.size(); i++) {
				if (playerList.get(i).equals(name)) {
					playerList.remove(i);
					i--;
				}
			}
			for (int i = 0; i < ctrlList.size(); i++) {
				if (ctrlList.get(i).equals(name)) {
					ctrlList.remove(i);
					i--;
				}
			}
			
		}
		
	}
	
	public void brodcastState(ArrayList<PlayerState> plist, ArrayList<BulletState> blist){
		try {
			Message m = new MessageWorldState(plist, blist, syncId);
			m.setReliable(false);
			server.broadcast(m);
		} catch (Exception e) {
			System.out.println("State brodcast failed.");
			e.printStackTrace();
		}
		syncId++;
		if (syncId >= 100000) {
			syncId = 0;
		}
	}
	
	public void disconnect() {
		server.close();
	}
	
	public void brodcastMap() {
		server.broadcast(new MessageWorldMap(map, score));
	}
	
	public ArrayList<MessagePlayerControll> getControlList() {
		return ctrlList;
	}
	
	public void setMap(String[][] map)  {
		this.map = map;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getPlayerList(){
		return (ArrayList<String>) playerList.clone();
	}

}
