package game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessageInitiziatePlayer extends AbstractMessage{
	public String name;

	public MessageInitiziatePlayer(){
	}
	
	public MessageInitiziatePlayer(String name){
		this.name = name;
	}
}
