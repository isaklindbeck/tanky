package game;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

@Serializable
public class BulletState {
	public int id;
	public Quaternion rot;
	public Vector3f pos;
	public int bounces;
	public float dmg;
	
	
	public BulletState(int id, Quaternion rot, Vector3f pos, int bounces, float dmg){
		this.id = id;
		this.rot = rot;
		this.pos = pos;
		this.bounces = bounces;
		this.dmg = dmg;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BulletState) return ((BulletState) obj).id == id;
		if (obj instanceof Bullet) return ((Bullet) obj).id == id;
		return false;
	}
}
