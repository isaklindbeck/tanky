package game;

import java.util.ArrayList;

import com.jme3.app.Application;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class BulletHandler {
	private Node rootNode;
	private int idcounter;
	private Application app;
	ArrayList<Bullet> list;
	
	public BulletHandler(Node rootNode, Application app){
		idcounter = 0;
		list = new ArrayList<Bullet>();
		this.rootNode = rootNode;
		this.app = app;
	}
	
	public void addBullet(String owner, Vector3f pos, Quaternion direction, int bounces, float dmg) {
		Bullet b = new Bullet(idcounter, pos, direction, rootNode, app, owner, bounces, dmg);
		idcounter++;
		if (idcounter == Integer.MAX_VALUE) idcounter = 0;
		list.add(b);
	}
	
	private void addBullet(BulletState state) {
		Bullet b = new Bullet(state.id, state.pos, state.rot, rootNode, app, "", state.bounces, state.dmg);
		list.add(b);
	}
	
	public void worldColission(World world) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).colissionDetect(world)) {
				list.get(i).kill();
				list.remove(i);
				i--;
			}
		}
	}
	
	public void playerColission(ArrayList<Player> playerList) {
		for (int i = 0; i < playerList.size(); i++) {
			for (int k = 0; k < list.size(); k++) {
				if (list.get(k).colissionDetect(playerList.get(i))) {
					if (playerList.get(i).shootPlayer(list.get(k).dmg)) {
						if (playerList.get(i).equals(list.get(k).owner)) {
							playerList.get(i).removePoint();
						} else {
							for (int l = 0; l < playerList.size(); l++) {
								if (playerList.get(l).equals(list.get(k).owner)) {
									int lvl = playerList.get(i).lvl;
									playerList.get(l).addPoint(lvl);
								}
							}
						}
					}
					list.get(k).kill();
					list.remove(k);
					k--;
				}
			}
		}
		
	}
	
	public ArrayList<BulletState> getStates() {
		ArrayList<BulletState> states = new ArrayList<BulletState>();
		for (int i = 0; i < list.size(); i++ ) {
			states.add(list.get(i).getState());
		}
		return states;
	}
	
	public void setStates(ArrayList<BulletState> states) {
		if (states == null) return;
		for (int i = 0; i < list.size(); i++) {
			if (!states.contains(list.get(i))) {
				//TODO tr�ff eller sistat stutts?
				list.get(i).playStutts();
				list.get(i).kill();
				list.remove(i);
				i--;
			}
		}
		for (int i = 0; i < states.size(); i++) {
			int index = list.indexOf(states.get(i));
			if (index >= 0) {
				list.get(index).setState(states.get(i));
			} else {
				addBullet(states.get(i));
			}
		}
		
		
//		for (int i = 0; i < list.size(); i++) {
//			list.get(i).kill();
//		}
//		list.clear();
//		for (int i = 0; i < states.size(); i++) {
//			//System.out.println("pos: " + states.get(i).pos + " rot: " + states.get(i).rot);
//			list.add(new TankyBullet(states.get(i).pos, states.get(i).rot, rootNode, assetManager, "", states.get(i).bounces, states.get(i).dmg));
//		}
	}
	
	public void updateState(float tpf) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).updateState(tpf);
		}
	}
}
