package game;

import java.util.ArrayList;
import java.util.Random;

import com.jme3.app.Application;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.input.controls.ActionListener;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.plugins.blender.BlenderModelLoader;
import com.jme3.scene.shape.Box;

public class Player extends Character implements ActionListener {
	private Node flNode, frNode, blNode, brNode;
	private boolean forward, backpedal, turnleft, turnright = false;
	private int numKey = 0;
	private boolean frontColission, rearColission = false;
	private Camera cam;
	private boolean shot = false;

	private boolean shoot = false;
	public Spatial cgeom;
	public String name;
	private BulletHandler bulletHandler;
	private int syncID = 0;
	public float cooldown;
	public float cooldownClock;
	public int score = 0;
	public float currentHealth = 20;
	public float fullHealth = 20;
	
	public int bounces = 1;
	public float dmg = 5;
	
	private Box healthBar;
	private Geometry testgeom;
	private float spawnTime = 5;
	public boolean dead = false;
	public ArrayList<Node> spawnNodes;
	private ParticleEmitter emitter;
	private ParticleEmitter lvlemitter;
	private AudioNode anDod;
	private AudioNode anHit;
	private AudioNode anLvl;
	
	private Application app; 
	
	public int pointsToLvl = 1;
	public int lvl = 1;
	public int lvlcounter = 0;

	public Player(Vector3f pos, Node rootNode, ArrayList<Node> spawnNodes ,Application app, Camera cam, String name, BulletHandler bulletHandler, boolean billboard) {
		super(pos, rootNode, app.getAssetManager());
		this.cam= cam;
		this.app = app;
		AssetManager assetMAanger = app.getAssetManager();
		this.bulletHandler = bulletHandler;
		this.name = name;
		this.spawnNodes = spawnNodes;
		cooldown = 1.5f;
		cooldownClock = 0f;
		healthBar = new Box(new Vector3f(0,0,0), 0.6f, 0.1f, 0.0f);
		testgeom = new Geometry("Box", healthBar);
		Material testMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		testMat.setColor("m_Color", ColorRGBA.Green);
        testgeom.setMaterial(testMat);
        
        
        emitter = new ParticleEmitter("Emitter", Type.Triangle,30);
        Material mat_red = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat_red.setTexture("Texture", assetManager.loadTexture("effects/broken.png"));
        emitter.setMaterial(mat_red);
        emitter.setStartSize(1f);
        emitter.setEnabled(true);
        emitter.setParticlesPerSec(0);
        
        mainNode.attachChild(emitter);
        
        
        lvlemitter = new ParticleEmitter("Emitter", Type.Triangle,1);
        Material lvlmat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        lvlmat.setTexture("Texture", assetManager.loadTexture("effects/lvlup.png"));
        
        lvlemitter.setMaterial(lvlmat);
        lvlemitter.setEnabled(true);
        lvlemitter.setParticlesPerSec(0);
        lvlemitter.setStartColor(ColorRGBA.White);
        lvlemitter.setEndColor(ColorRGBA.White);
        lvlemitter.setLowLife(0.8f);
        lvlemitter.setRotateSpeed(16);
        lvlemitter.setHighLife(0.8f);
        lvlemitter.setEndSize(1.5f);
        lvlemitter.setStartSize(0.5f);
        lvlemitter.setInWorldSpace(false);
        mainNode.attachChild(lvlemitter);
        
        
        anDod = new AudioNode(assetMAanger, "sounds/kill.wav", false);
        anLvl = new AudioNode(assetMAanger, "sounds/lvlup.wav", false);
        anLvl.setVolume(0.3f);
        anHit = new AudioNode(assetMAanger, "sounds/hit.wav", false);
        mainNode.attachChild(anDod);
        mainNode.attachChild(anLvl);
        mainNode.attachChild(anHit);
        
		if(billboard) setBillBoard(mainNode);
		
		if (spawnNodes != null) {
			Random rand = new Random();
			int i = rand.nextInt(spawnNodes.size());
			mainNode.setLocalTranslation(spawnNodes.get(i).getLocalTranslation().add(0, 1, 0));
		}
		
	}

	@Override
	protected void initModel() {
		
		brNode = new Node();
        brNode.move(new Vector3f(-0.5f,0.5f,0.5f));
        mainNode.attachChild(brNode);
                
		blNode = new Node();
        blNode.move(new Vector3f(0.5f,0.5f,0.5f));
        mainNode.attachChild(blNode);
                
		frNode = new Node();
        frNode.move(new Vector3f(0.5f,0.5f,-0.5f));
        mainNode.attachChild(frNode);
                
		flNode = new Node();
        flNode.move(new Vector3f(-0.5f,0.5f,-0.5f));
        mainNode.attachChild(flNode);     
        
        assetManager.registerLoader(BlenderModelLoader.class, "blend");
        Spatial result = assetManager.loadModel("models/untitled.blend");

        Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        mat.setFloat("m_Shininess", 5f);
        
        Material ballmat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        ballmat.setFloat("m_Shininess", 5f);
//      ballmat.setTexture("DiffuseMap", assetManager.loadTexture("models/textures/ball_color.png"));
        ballmat.setTexture("GlowMap", assetManager.loadTexture("models/textures/ball_emit.png"));
        
        
        
//        assetManager.registerLoader(BlenderLoader.class, "blend");
//        LoadingResults load = (LoadingResults) assetManager.loadModel("models/untitled.blend");
//        Node result = new Node();
//        for (Spatial obj : load.getObjects()) {
//        	result.attachChild(obj);
//        	if (obj.getName().equals("Sphere")) {
//        		obj.setMaterial(ballmat);
//        	} else {
//        		obj.setMaterial(mat);
//        	}
//        }
        
        result.setLocalTranslation(0, -1f, 0);
        result.rotate(0,(float)Math.PI,0);        
        result.setMaterial(mat);
        
        mainNode.attachChild(result);
        cgeom = result;
        cgeom.setName(name);
        
        
	}
	
	public void setAction(MessagePlayerControll ctrl) {
		this.forward = ctrl.forwards;
		this.backpedal = ctrl.backwards;
		this.turnleft = ctrl.turnLeft;
		this.turnright = ctrl.turnRight;
		this.shoot = ctrl.shoot;
		this.numKey = ctrl.numKey;
	}
	
	public MessagePlayerControll getControl() {
		MessagePlayerControll ctrl = new MessagePlayerControll(this.name, this.syncID, this.forward, this.backpedal, this.turnleft, this.turnright, this.shoot, this.numKey);
		numKey = 0;
		return ctrl;
	}

	public void onAction(String action, boolean value, float tpf) {
        if(action.equals("forward")) {
        	 if (value) {
        		 forward = true;
        	 }  else {
        		 forward = false;
        	 }
        }
        else if(action.equals("backpedal")) {
       	 if (value) {
       		 backpedal = true;
    	 }  else {
    		 backpedal = false;
    	 }
        }
        else if(action.equals("turnleft")) {
       	 if (value) {
       		turnleft = true;
    	 }  else {
    		 turnleft = false;
    	 }
        }
        else if(action.equals("turnright")) {
       	 if (value) {
       		turnright = true;
    	 }  else {
    		 turnright = false;
    	 }
        }
        else if(action.equals("shoot")) {
          	 if (value) {
             	 shoot = true;
         	 } else {
         		 shoot = false;
         	 }
        }
        else if(action.equals("1")) {
        	if (value) numKey = 1;
        }
        else if(action.equals("2")) {
        	if (value) numKey = 2; 
        }
        else if(action.equals("3")) {
        	if (value) numKey = 3;
        }
        else if(action.equals("4")) {
        	if (value) numKey = 4;
        }
        else if(action.equals("5")) {
        	if (value) {
        		numKey = 5;
        		lvlemitter.emitAllParticles();
				if (app.getAudioRenderer() != null) {
					anLvl.setLocalTranslation(mainNode.getLocalTranslation());
					anLvl.playInstance();
				}
        	}
        }
	}
	
	private void shoot() {		
		bulletHandler.addBullet(this.name, mainNode.getLocalTranslation().subtract(0, 0.2f, 0), mainNode.getWorldRotation(), bounces, dmg);
		shot = true;
	}
	
	public PlayerState getState(){
		boolean boom = false;
		if (shot) {
			shot = false;
			boom = true;
		}
		return new PlayerState(name, mainNode.getWorldRotation(), mainNode.getWorldTranslation(), boom, forward, dead, currentHealth, fullHealth, cooldown, cooldownClock, dmg, bounces, score, lvlcounter);
	}
	
	public void colissionDetect(World world){
        if (world.containsNode(flNode) || world.containsNode(frNode)) {
        	frontColission = true;
        } else {
        	frontColission = false;
        }
        if (world.containsNode(blNode) || world.containsNode(brNode)) {
        	rearColission = true;
        } else {
        	rearColission = false;
        }
	}
	
	public void setState(PlayerState state){
		if (state.pos != null && state.rot != null) {
			if (currentHealth != state.currentHealth) {
				currentHealth = state.currentHealth;
					if (currentHealth < state.currentHealth && app.getAudioRenderer() != null) {
						anHit.setLocalTranslation(mainNode.getLocalTranslation());
						anHit.playInstance();
					}
				adjustHealthBar(currentHealth/fullHealth);
			}
			
			mainNode.setLocalTranslation(state.pos);
			mainNode.setLocalRotation(state.rot);
			fullHealth = state.fullHealth;
			cooldown = state.cooldown;
			cooldownClock = state.cooldownClock;
			bounces = state.bounces;
			dmg = state.dmg;
			
			if (!dead && state.dead && app.getAudioRenderer() != null) {
				anDod.setLocalTranslation(mainNode.getLocalTranslation());
				anDod.playInstance();
			}
			
			dead = state.dead;
//			System.out.println(state.lvlcounter);
			if (lvlcounter < state.lvlcounter) {
				lvl++;
				lvlemitter.emitAllParticles();
				if (app.getAudioRenderer() != null) {
					anLvl.setLocalTranslation(mainNode.getLocalTranslation());
					anLvl.playInstance();
				}
			}
			lvlcounter = state.lvlcounter;
			score = state.score;
			
			if (dead) {
				emitter.emitAllParticles();
			}
		}
	}
	
	public void updateState(float tpf){
		cooldownClock += tpf;
		if(dead) {
			spawnTime -= tpf;
			mainNode.move(0, -0.8f*tpf, 0);
			emitter.emitAllParticles();
			if (spawnTime < 0) {
				Random rand = new Random();
				int i = rand.nextInt(spawnNodes.size());
				mainNode.setLocalTranslation(spawnNodes.get(i).getLocalTranslation().add(0, 1, 0));
				dead = false;
				
			} else {
				return;
			}
		}	
		
		rootNode.updateGeometricState();
		if (forward && !frontColission) {			
			mainNode.move(mainNode.getLocalRotation().getRotationColumn(2).normalize().mult(-tpf*8));
		}
		if (backpedal && !rearColission){
			mainNode.move(mainNode.getLocalRotation().getRotationColumn(2).normalize().mult(tpf*4));
		}
		if (turnleft) {
//			if (shoot) tpf = tpf / 3;
			mainNode.rotate(0, 2f*tpf, 0);
		}
		if (turnright) {
//			if (shoot) tpf = tpf / 3;
			mainNode.rotate(0, -2f*tpf, 0);
		}
		//shoot + cooldown
		cooldownClock += tpf;
		if (cooldownClock >= cooldown && shoot) {
			shoot();
			cooldownClock = 0;
		}
		
		if (numKey == 1 && lvlcounter > 0) {
			int plusH = (int) (fullHealth/4);
			fullHealth = fullHealth + plusH;
			currentHealth += plusH;
			lvlcounter--;
		} else if (numKey == 2 && lvlcounter > 0) {
			dmg = dmg + 2 + (dmg / 10);
			lvlcounter--;
		} else if (numKey == 3 && lvlcounter > 0) {
			bounces++;
			lvlcounter--;
		} else if (numKey == 4 && lvlcounter > 0) {
			cooldown = cooldown * 0.875f;
			lvlcounter--;
		} else if (numKey == 5) {
			lvl++;
			lvlcounter++;
			fullHealth+=3;
			currentHealth+=3;
    		lvlemitter.emitAllParticles();
			if (app.getAudioRenderer() != null) {
				anLvl.setLocalTranslation(mainNode.getLocalTranslation());
				anLvl.playInstance();
			}
		}
		numKey = 0;
		
	}
	
	public void updateCam() {
		if (!dead) {
			cam.setLocation(mainNode.getLocalTranslation().add(mainNode.getLocalRotation().getRotationColumn(2).normalize().mult(3)).add(0, 1.5f, 0));
			cam.lookAt(mainNode.getLocalTranslation().add(0, 0, 0), new Vector3f(0, 0, 0));
			
		} else {
			cam.setLocation(cam.getLocation().add(0, 0.2f, 0));
			cam.lookAt(mainNode.getLocalTranslation(), new Vector3f(0, 0, 0));
		}
	}
	
	public boolean equals(String name) {
		return this.name.equals(name);
	}
	
	public boolean equals(MessagePlayerControll ctrl) {
		System.out.println("check at player");
		return this.name.equals(ctrl.playerName);
	}

	public boolean equals(Player player) {
		return this.name.equals(player.name);
	}
	
	public boolean containsNode(Node node) {
		if (cgeom.getWorldBound().contains(node.getWorldTranslation())) {
			//System.out.println(this.name + " contains node");
		}
		return cgeom.getWorldBound().contains(node.getWorldTranslation()) && !dead;
	}
	
	public boolean shootPlayer(float dmg) {
		
		currentHealth -= dmg;
		float healthStatus = currentHealth/fullHealth;
		adjustHealthBar(healthStatus);
		if (app.getAudioRenderer() != null) {
			anHit.setLocalTranslation(mainNode.getLocalTranslation());
			anHit.playInstance();
		}
		if (currentHealth <= 0) {
			killPlayer();
			currentHealth = fullHealth;
			testgeom.setLocalScale(1f, 1f, 0);
			testgeom.getMaterial().setColor("m_Color", ColorRGBA.Green);
			return true;
		}
		return false;
	}
	
	private void adjustHealthBar(float healthStatus) {
		testgeom.setLocalScale(healthStatus*1f, 1f, 0);
//		System.out.println("Health: " + currentHealth);

		ColorRGBA col;
		if (healthStatus > 0.5) {
			col = new ColorRGBA((1 - (healthStatus-0.5f)*2),1,0, 1);
		} else {
			col = new ColorRGBA(1,healthStatus*2,0, 1);
		}
		
		testgeom.getMaterial().setColor("m_Color", col);
	}
	
	private void killPlayer() {
		if (app.getAudioRenderer() != null) {
			anDod.setLocalTranslation(mainNode.getLocalTranslation());
			anDod.playInstance();
		}
		emitter.emitAllParticles();
		dead = true;
		spawnTime = 5;
	}
	
	public void addPoint(int oplvl) {
		int p = oplvl/2 + oplvl % 2; 
		score++;
		pointsToLvl = pointsToLvl - p;
		if(pointsToLvl <= 0) {
			lvlcounter++;
			fullHealth+=5;
			currentHealth+=5;
			lvl++;
			lvlemitter.emitAllParticles();
			if (app.getAudioRenderer() != null) {
				anLvl.setLocalTranslation(mainNode.getLocalTranslation());
				anLvl.playInstance();
			}
			pointsToLvl = lvl;
			
			
		}
	}
	
	public void removePoint() {
		score--;
		//System.out.println("-1 Total points: " + pointCount + " for player" + name);
	}
	
	public void setBillBoard(Node barNode) {		
        BillboardControl bc = new BillboardControl();
		bc.setAlignment( BillboardControl.Alignment.Screen );
		testgeom.addControl(bc);
		testgeom.move(0, 1, 0);
		barNode.attachChild(testgeom);
		
		BitmapFont font = assetManager.loadFont("Interface/Fonts/Default.fnt");
		BitmapText text = new BitmapText(font, false);
		text.setSize(0.4f);
		text.setColor(ColorRGBA.White);
		text.setText(name);
		text.setAlignment( BitmapFont.Align.Left);
		float textWidth = text.getLineWidth();
		float textHight = text.getHeight();
        float textOffset = textWidth / 2;
        text.setBox(new Rectangle(-textOffset,0, textWidth, textHight));
        text.setQueueBucket( Bucket.Transparent );
        text.move(0, 1.6f, 0);
        
		BillboardControl tbc = new BillboardControl();
		tbc.setAlignment(BillboardControl.Alignment.Screen);
		text.addControl(tbc);
		
		barNode.attachChild(text);
	}
}
