package game;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public abstract class Character {
	Node mainNode;
	Node rootNode;
	AssetManager assetManager;
	
	public Character (Vector3f pos, Node rootNode, AssetManager assetManager){
		mainNode = new Node();
		mainNode.move(pos);
		this.rootNode = rootNode;
		this.assetManager = assetManager;
		initModel();
		
	}
	
	public void initTankyChar() {
		rootNode.attachChild(mainNode);
	}
	
	public Node getMainNode(){
		return mainNode;
	}
	
	protected abstract void initModel();

}
