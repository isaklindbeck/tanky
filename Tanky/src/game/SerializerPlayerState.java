package game;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializer;
import com.jme3.network.serializing.serializers.BooleanSerializer;
import com.jme3.network.serializing.serializers.StringSerializer;
import com.jme3.network.serializing.serializers.Vector3Serializer;

public class SerializerPlayerState extends Serializer{

	@SuppressWarnings("unchecked")
	public PlayerState readObject(ByteBuffer data, @SuppressWarnings("rawtypes") Class state) throws IOException {
		String name = (String) StringSerializer.readClassAndObject(data);
		Vector3f vector = (Vector3f) Vector3Serializer.readClassAndObject(data);
		float w = data.getFloat();
		float x = data.getFloat();
		float y = data.getFloat();
		float z = data.getFloat();
		Quaternion q = new Quaternion(x, y, z, w);
		boolean shoot = (Boolean) BooleanSerializer.readClassAndObject(data);
		boolean forwards = (Boolean) BooleanSerializer.readClassAndObject(data);
		boolean dead = (Boolean) BooleanSerializer.readClassAndObject(data);
		float currentHealth = data.getFloat();
		float fullHealth = data.getFloat();
		float cooldown = data.getFloat();
		float cooldownClock = data.getFloat();
		float dmg = data.getFloat();
		int bounces = data.getInt();
		int score = data.getInt();
		int lvlc = data.getInt();
		
		PlayerState playerState = new PlayerState(name, q, vector, shoot, forwards, dead, currentHealth, fullHealth, cooldown, cooldownClock, dmg, bounces, score, lvlc);
		return playerState;
	}

	@Override
	public void writeObject(ByteBuffer buffert, Object state) throws IOException {
		//buffert.clear();
		//System.out.println(buffert.remaining());
		StringSerializer.writeClassAndObject(buffert, ((PlayerState)state).name);
		Vector3Serializer.writeClassAndObject(buffert, ((PlayerState)state).pos);
		buffert.putFloat(((PlayerState)state).rot.getW());
		buffert.putFloat(((PlayerState)state).rot.getX());
		buffert.putFloat(((PlayerState)state).rot.getY());
		buffert.putFloat(((PlayerState)state).rot.getZ());
		BooleanSerializer.writeClassAndObject(buffert, ((PlayerState)state).shoot);
		BooleanSerializer.writeClassAndObject(buffert, ((PlayerState)state).forwards);
		BooleanSerializer.writeClassAndObject(buffert, ((PlayerState)state).dead);
		buffert.putFloat(((PlayerState)state).currentHealth);
		buffert.putFloat(((PlayerState)state).fullHealth);
		buffert.putFloat(((PlayerState)state).cooldown);
		buffert.putFloat(((PlayerState)state).cooldownClock);
		buffert.putFloat(((PlayerState)state).dmg);
		buffert.putInt(((PlayerState)state).bounces);
		buffert.putInt(((PlayerState)state).score);
		buffert.putInt(((PlayerState)state).lvlcounter);
		
	}

}
