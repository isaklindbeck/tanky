package game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessageWorldMap extends AbstractMessage{
	public String[][] map;
	public int score;
	
	public MessageWorldMap(){
	}
	
	public MessageWorldMap(String[][] map, int score){
		this.map = map;
		this.score = score;
	}

}
