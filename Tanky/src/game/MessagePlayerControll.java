package game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessagePlayerControll extends AbstractMessage{
	public String playerName;
	public int syncID;
	public boolean forwards;
	public boolean backwards;
	public boolean turnLeft;
	public boolean turnRight;
	public boolean shoot;
	public int numKey;
	
	public MessagePlayerControll() {
		
	}
	
	public MessagePlayerControll(String playerName, int syncID, boolean forwards, boolean backwards, boolean turnLeft, boolean turnRight, boolean shoot, int numKey) {
		this.playerName = playerName;
		this.syncID = syncID;
		this.forwards = forwards;
		this.backwards = backwards;
		this.turnLeft = turnLeft;
		this.turnRight = turnRight;
		this.shoot = shoot;
		this.numKey = numKey;
	}
	
	public void update(MessagePlayerControll ctrl) {
		if (ctrl.playerName.equals(this.playerName)) {
			this.syncID = ctrl.syncID;
			this.forwards = ctrl.forwards;
			this.backwards = ctrl.backwards;
			this.turnLeft = ctrl.turnLeft;
			this.turnRight = ctrl.turnRight;
			this.shoot = ctrl.shoot;
			this.numKey = ctrl.numKey;
		}
	}
	
	public void setSyncId(int id) {
		this.syncID = id;
	}
	
	public boolean equals(Object obj) {
		if (obj instanceof String) return this.playerName.equals((obj));
		if (obj instanceof Player) return this.playerName.equals(((Player)obj).name);
		if (obj instanceof PlayerState) return this.playerName.equals(((MessagePlayerControll)obj).playerName);

		return false;
	}

}
