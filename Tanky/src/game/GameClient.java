package game;

import java.io.IOException;
import java.util.ArrayList;

import com.jme3.network.Client;
import com.jme3.network.ClientStateListener;
import com.jme3.network.ClientStateListener.DisconnectInfo;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;

public class GameClient implements MessageListener<Client>, ClientStateListener{
	private Client client;
	private ArrayList<PlayerState> stateList;
	private ArrayList<BulletState> bulletStateList;
	private String[][] map;
	private String playerName;
	private ArrayList<String> playerList;
	public boolean worldLoaded = false;
	private int syncId;
	private int worldSync;
	public String score = "Score:";
	public String playersScore = "Player:";
	private boolean connected;
	public int scoreGoal;
	public String lastError = ""; 
	
	public GameClient(int port, String ip, String playerName) throws IOException {
		stateList = new ArrayList<PlayerState>();
		playerList = new ArrayList<String>();
		this.playerName = playerName;
		this.syncId = 0;
		this.worldSync = 0;
		stateList.add(new PlayerState(playerName));
		playerList.add(playerName);
		
	    try {
			client = Network.connectToServer(ip, port);
			GameServer.initzializeClasses();
		    client.addMessageListener(this, MessageWorldMap.class);
		    client.addMessageListener(this, MessageWorldState.class);
		    client.addMessageListener(this, MessageInitiziatePlayer.class);
		    client.addClientStateListener(this);
	    	client.start();
	    	connected = true;
		} catch (IOException e) {
			System.out.println("Failed to initiate client.");
			throw e;
		}
	}

	@Override
	public void messageReceived(Client arg0, Message arg1) {

		if (arg1 instanceof MessageWorldMap) {
			System.out.println("Recived map");
			map = ((MessageWorldMap)arg1).map;
			scoreGoal = ((MessageWorldMap)arg1).score;
		}
		
		if (arg1 instanceof MessageWorldState) {
			ArrayList<PlayerState> state = ((MessageWorldState)arg1).playerStates;

			if (((MessageWorldState)arg1).syncId > this.worldSync) {
				this.bulletStateList = ((MessageWorldState)arg1).bulletStates;
				this.worldSync = ((MessageWorldState)arg1).syncId;
				
				for (int i = 0; i < state.size(); i++) {

					if (stateList.contains(state.get(i))) {
						int index = stateList.indexOf(state.get(i));
						stateList.set(index, interpolate(stateList.get(index), state.get(i)));
					}
				}
			}
		}
		if (arg1 instanceof MessageInitiziatePlayer) {
			if (!playerList.contains(((MessageInitiziatePlayer) arg1).name)) {
				System.out.println("Initziating player: " + ((MessageInitiziatePlayer) arg1).name);
				playerList.add(((MessageInitiziatePlayer) arg1).name);
				stateList.add(new PlayerState(((MessageInitiziatePlayer) arg1).name));
			}
		}
	}
	
	public PlayerState interpolate(PlayerState s1, PlayerState s2) {
		PlayerState ps = s2.cloneState();
		ps.pos = s1.pos.interpolate(s2.pos, 0.25f);
		if (s2.rot != null && s1.rot != null) ps.rot = s1.rot.add(s2.rot).mult(0.5f);
		return ps;
	}
	
	public void sendControls(MessagePlayerControll ctrlMsg) {
		if (client == null) return;
		ctrlMsg.setSyncId(syncId);
		syncId++;
		if (syncId >= 100000) {
			syncId = 1;
		}
		ctrlMsg.setReliable(false);
		client.send(ctrlMsg);
	}
	
	public String[][] getMap() {
		return map;
	}
	
	public int getScoreGoal() {
		return scoreGoal;
	}
	
	public ArrayList<PlayerState> readState(){
		return stateList;
	}
	
	public ArrayList<String> getPlayerList(){
		return playerList;
	}
	
	public ArrayList<BulletState> getBulletStates() {
		return this.bulletStateList;
	}

	@Override
	public void clientConnected(Client arg0) {
		client.send(new MessageInitiziatePlayer(playerName));
	}
	
	public void clientDisconnect() {
		client.send(new MessageDisconnectPlayer(playerName));
		connected = false;
	}

	@Override
	public void clientDisconnected(Client arg0, DisconnectInfo info) {
		lastError = info.reason;
		connected = false;
		client.close();
	}
	
	public boolean isConnected() {
		return connected;
	}


}
