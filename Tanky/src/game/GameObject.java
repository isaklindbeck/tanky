package game;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Triangle;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

public abstract class GameObject {
	protected Node mainNode;
	protected Node rootNode;
	protected AssetManager assetManager;
	protected Geometry geo;
	
	public GameObject (Vector3f pos, Node rootNode, AssetManager assetManager){
		mainNode = new Node();
		mainNode.move(pos);
		this.rootNode = rootNode;
		this.assetManager = assetManager;
		initModel();
	}
	public void initTankyObject() {
		rootNode.attachChild(mainNode);
	}

	public boolean containsNode(Node node) {
		return geo.getWorldBound().contains(node.getWorldTranslation());
	}

	public void roateTankyObject(){
		mainNode.rotate(0, FastMath.PI/2, 0);
		
	}
	
	protected abstract void initModel();
	
	public void bounce(Node node) {
		Vector3f v = new Vector3f();
		float x = node.getWorldTranslation().getX()-mainNode.getWorldTranslation().getX();
		float z = node.getWorldTranslation().getZ()-mainNode.getWorldTranslation().getZ();
		Vector3f origin = new Vector3f(x,1,z);
		
		Quaternion d = mainNode.getWorldRotation();
		d.inverse();
		
		d = d.inverse();
		origin = d.mult(origin);
		
		Vector3f direction = node.getLocalRotation().getRotationColumn(2).normalize().mult(1);
		direction =d.mult(direction);
		Ray ray = new Ray(origin, direction);		
		
		Triangle tri = new Triangle();
		int i = geo.getMesh().getTriangleCount();
		Quaternion q = new Quaternion();
		Vector3f dir = node.getLocalRotation().getRotationColumn(2).normalize().mult(-1);
//		Vector3f point = node.getLocalTranslation();
		for (int k = 0; k<i; k++){
			geo.getMesh().getTriangle(k, tri);
			if (ray.intersectWhere(tri, v)){
				Vector3f normal = mainNode.getWorldRotation().mult(tri.getNormal());				
				Vector3f proj = dir.project(normal);
				Vector3f v2 = dir.subtract(proj).mult(-2);
				Vector3f newdir = dir.add(v2);
				q.lookAt(newdir, new Vector3f(0,1,0));
			}
			tri = new Triangle();
			
		}
		q.add(mainNode.getWorldRotation());
		node.move(node.getLocalRotation().getRotationColumn(2).normalize().mult(0.2f));
//		point = mainNode.getWorldTranslation().add(point);
//		node.setLocalTranslation(node.getWorldTranslation().add(point));
//		node.setLocalTranslation(node.getLocalTranslation().subtract(point));
		
		node.setLocalRotation(q);
	}
}