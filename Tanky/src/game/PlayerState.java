package game;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;

@Serializable
public class PlayerState {
	public String name;
	public Quaternion rot;
	public Vector3f pos;
	public boolean shoot;
	
	public boolean forwards;
	public boolean backwards;
	public boolean turnLeft;
	public boolean turnRight;
	
	public boolean dead;
	public float currentHealth;
	public float fullHealth;
	
	public float dmg;
	public float cooldownClock;
	public float cooldown;
	public int bounces;
	
	public int score;
	
	public int lvlcounter;
	
	public PlayerState(String name, Quaternion rot, Vector3f pos, boolean shoot, boolean forwards, boolean dead, float currentHealth, float fullHealth, float cooldown, float cooldownClock, float dmg, int bounces, int score, int lvlcounter){
		this.name = name;
		this.rot = rot;
		this.pos = pos;
		this.shoot = shoot;
		this.forwards = forwards;
		this.dead = dead;
		this.currentHealth = currentHealth;
		this.fullHealth = fullHealth;
		this.score = score;
		this.dmg = dmg;
		this.cooldown = cooldown;
		this.cooldownClock = cooldownClock;
		this.bounces = bounces;
		this.lvlcounter = lvlcounter;
	}
	
	public PlayerState(String name){
		this.name = name;
		this.pos = new Vector3f(0,0,0);
	}
	
	public PlayerState cloneState() {
		return new PlayerState(name, rot, pos, shoot, forwards, dead, currentHealth, fullHealth, cooldown, cooldownClock, dmg, bounces, score, lvlcounter);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) return this.name.equals((obj));
		if (obj instanceof Player) return this.name.equals(((Player)obj).name);
		if (obj instanceof PlayerState) return this.name.equals(((PlayerState)obj).name);
		return false;
	}
}
