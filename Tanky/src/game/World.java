package game;

import java.util.ArrayList;
import java.util.Random;

import com.jme3.asset.AssetManager;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

@Serializable
public class World{
	public Node mainNode;
	private Node rootNode;
	private AssetManager assetManager;
	private ArrayList<GameObject> objList = new ArrayList<GameObject>();
	private int width, height;
	public String[][] map;
	private ArrayList<Node> spawnNodes;
	private ArrayList<PointOfInterest> navPoints;

	public World(Node rootNode, AssetManager assetManager, int width, int height) {
		mainNode = new Node();
		this.rootNode = rootNode;
		this.assetManager = assetManager;
		this.width = width;
		this.height = height;
		this.map = new String[width][height];
		generateGround(width,height);
		generateMap();
		loadObject(this.map);
		loadLights(width,height);
		spawnNodes = new ArrayList<Node>();
		navPoints = new ArrayList<PointOfInterest>();
		loadSpawnNodes(this.map);
	}

	public World(Node rootNode, AssetManager assetManager, String[][] newMap) {
		mainNode = new Node();
		this.rootNode = rootNode;
		this.assetManager = assetManager;		
		this.width = newMap.length;
		this.height = newMap[0].length;
		this.map = new String[newMap.length][newMap[0].length];
		generateGround(width,height);
		this.map = newMap;
		//generateEmptyMap(newMap);
		loadObject(this.map);
		loadLights(width,height);

	}

	public void generateGround(int x, int y){
		Material floor_mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
		floor_mat.setFloat("m_Shininess", 5f);
		Box floorBox = new Box(Vector3f.ZERO, x, 1f, y);
		Geometry floor = new Geometry("floor", floorBox);
		floor.setMaterial(floor_mat);
		floor.move(0, -1, 0);
		mainNode.attachChild(floor);

	}
	public void generateMap(){
		for (int i = 0; i < map.length; i++){
			for (int k = 0; k < map[0].length; k++){
				map[i][k] = " ";
			}
		}

		for (int i = 0; i < map[0].length; i++){
			map[0][i] = "V";
			map[map.length-1][i] = "V";
		}

		for (int i = 0; i < map.length; i++){
			map[i][0] = "H";
			map[i][map[0].length-1] = "H";
		}
		map[0][0] = "X";
		map[map.length-1][0] = "X";
		map[0][map[0].length-1] = "X";
		map[map.length-1][map[0].length-1] = "X";

		String[][] inside = this.recursiveGenerator(new String[map.length-2][map[0].length-2]);
		for (int i = 0; i < inside.length; i++) {
			for (int k = 0; k < inside[0].length; k++) {
				map[i+1][k+1] = inside[i][k];
			}
		}

		for (int i = 0; i < map.length; i++) {
			if (map[i][1].equals("V")) {
				map[i][0] = "X";
			}

			if (map[i][map[0].length-2].equals("V")) {
				map[i][map[0].length-1] = "X";
			}

		}

		for (int i = 0; i < map[0].length; i++) {
			if (map[1][i].equals("H")) {
				map[0][i] = "X";
			}
			if (map[map.length-2][i].equals("H")) {
				map[map.length-1][i] = "X";
			}
		}

		for (int i = 1; i < map.length-1; i++){
			for (int k = 1; k < map[0].length-1; k++) {
				if (map[i][k].equals("H") && (map[i][k-1].equals("V") || map[i][k+1].equals("V"))) {
					map[i][k] = "X";
				}
				if (map[i][k].equals("V") && (map[i-1][k].equals("H") || map[i+1][k].equals("H"))) {
					map[i][k] = "X";
				}
				//TODO
				if (map[i][k].equals("P")) {
					boolean b = true;
					int x = i;
					int y = k;
					while (b) {
						b = false;
						x++;
						if (map[x][y+1].equals("V") || map[x][y-1].equals("V") || map[x][y+1].equals("X") || map[x][y-1].equals("X")) {

						} else if (map[x][y].equals(" ")) {
							map[x][y] = "M";
							b = true;
						} else if (map[x][y].equals("S")) {
							map[x][y] = "M";
							map[x][y+1] = "S";
							b = true;
						} else if (map[x][y].equals("P")) {

						}  else if (map[x][y].equals("M") && !map[x+1][y].equals("M")) {
							map[x][y] = "B";
							b = true;
						}  else if (map[x][y].equals("M")) {
							b = true;
						}else if (map[x][y].equals("B")) {
							b = true;
						}
					}
					b = true;
					x = i;
					y = k;
					while (b) {
						b = false;
						x--;
						if (map[x][y+1].equals("V") || map[x][y-1].equals("V") || map[x][y+1].equals("X") || map[x][y-1].equals("X")) {

						} else if (map[x][y].equals(" ")) {
							map[x][y] = "M";
							b = true;
						} else if (map[x][y].equals("S")) {
							map[x][y] = "M";
							map[x][y+1] = "S";
							b = true;
						} else if (map[x][y].equals("P")) {

						}  else if (map[x][y].equals("M") && !map[x-1][y].equals("M")) {
							map[x][y] = "B";
							b = true;
						}  else if (map[x][y].equals("M")) {
							b = true;
						}  else if (map[x][y].equals("B")) {
							b = true;
						}
					}

					b = true;
					x = i;
					y = k;
					while (b) {
						b = false;
						y++;
						if (map[x+1][y].equals("H") || map[x-1][y].equals("H") || map[x+1][y].equals("X") || map[x-1][y].equals("X")) {

						} else if (map[x][y].equals(" ")) {
							map[x][y] = "M";
							b = true;
						} else if (map[x][y].equals("S")) {
							map[x][y] = "M";
							map[x+1][y] = "S";
							b = true;
						} else if (map[x][y].equals("P")) {

						}  else if (map[x][y].equals("M") && !map[x][y+1].equals("M")) {
							map[x][y] = "B";
							b = true;
						}  else if (map[x][y].equals("M")) {
							b = true;
						}  else if (map[x][y].equals("B")) {
							b = true;
						}
					}
					b = true;
					x = i;
					y = k;
					while (b) {
						b = false;
						y--;
						if (map[x+1][y].equals("H") || map[x-1][y].equals("H") || map[x+1][y].equals("X") || map[x-1][y].equals("X")) {

						} else if (map[x][y].equals(" ")) {
							map[x][y] = "M";
							b = true;
						} else if (map[x][y].equals("S")) {
							map[x][y] = "M";
							map[x+1][y] = "S";
							b = true;
						} else if (map[x][y].equals("P")) {

						}  else if (map[x][y].equals("M") && !map[x][y-1].equals("M")) {
							map[x][y] = "B";
							b = true;
						}  else if (map[x][y].equals("M")) {
							b = true;
						}  else if (map[x][y].equals("B")) {
							b = true;
						}
					}







				}
			}	
		}
		for (int i = 1; i < map.length-1; i++){
			for (int k = 1; k < map[0].length-1; k++) {
				if (map[i][k].equals("M")) {
					map[i][k] = " ";
				}else if (map[i][k].equals("B")) {
					map[i][k] = "P";
				}

				if (map[i][k].equals("P")) {
					//					if (map[i+1][k].equals("V") || map[i+1][k].equals("H") || map[i+1][k].equals("X")) map[i+1][k] = " ";
					//					if (map[i+1][k+1].equals("V") || map[i+1][k+1].equals("H") || map[i+1][k+1].equals("X")) map[i+1][k+1] = " ";
					//					if (map[i][k+1].equals("V") || map[i][k+1].equals("H") || map[i][k+1].equals("X")) map[i][k+1] = " ";
					//					if (map[i-1][k+1].equals("V") || map[i-1][k+1].equals("H") || map[i-1][k+1].equals("X")) map[i-1][k+1] = " ";
					//
					//					if (map[i-1][k].equals("V") || map[i-1][k].equals("H") || map[i-1][k].equals("X")) map[i-1][k] = " ";
					//					if (map[i-1][k-1].equals("V") || map[i-1][k-1].equals("H") || map[i-1][k-1].equals("X")) map[i-1][k-1] = " ";
					//					if (map[i][k-1].equals("V") || map[i][k-1].equals("H") || map[i][k-1].equals("X")) map[i+1][k-1] = " ";
					//					if (map[i+1][k-1].equals("V") || map[i+1][k-1].equals("H") || map[i+1][k-1].equals("X")) map[i+1][k-1] = " ";

				}
			}
		}

		//System.out.println(this.toString());
	}

	private String[][] recursiveGenerator(String[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int k = 0; k < matrix[0].length; k++) {
				matrix[i][k] = " ";
			}
		}

		int size = matrix.length * matrix[0].length;
		//System.out.println(size);
		Random rand = new Random();
		int n = rand.nextInt(size);
		if (n < 12 || matrix.length < 10 || matrix[0].length < 10){
			matrix[matrix.length/2][matrix[0].length/2] = "S";
			return matrix;
		}

		int hWall = 4 + rand.nextInt(matrix.length - 8);
		int vWall = 4 + rand.nextInt(matrix[0].length - 8);

		//TODO Fixa s� inte v�gg byggs i d�rr.

		String[][] q1 = recursiveGenerator(new String[hWall][vWall]);
		String[][] q2 = recursiveGenerator(new String[hWall][matrix[0].length-vWall]);
		String[][] q3 = recursiveGenerator(new String[matrix.length-hWall][vWall]);
		String[][] q4 = recursiveGenerator(new String[matrix.length-hWall][matrix[0].length-vWall]);

		for (int i = 0; i < q1.length; i++) {
			for (int k = 0; k < q1[0].length; k++) {
				matrix[i][k] = q1[i][k];
			}
		}

		for (int i = 0; i < q2.length; i++) {
			for (int k = 0; k < q2[0].length; k++) {
				matrix[i][k+vWall] = q2[i][k];
			}
		}

		for (int i = 0; i < q3.length; i++) {
			for (int k = 0; k < q3[0].length; k++) {
				matrix[i+hWall][k] = q3[i][k];
			}
		}

		for (int i = 0; i < q4.length; i++) {
			for (int k = 0; k < q4[0].length; k++) {
				matrix[i+hWall][k+vWall] = q4[i][k];
			}
		}
		//System.out.println("hW: " + hWall + " vW: " + vWall + " hL: " + matrix.length + " vL: " + matrix[0].length);

		for (int k = 0; k < matrix.length; k++) {
			//System.out.println("nr: " + k);
			matrix[k][vWall] = "H";
		}

		for (int k = 0; k < matrix[0].length; k++) {
			//System.out.println("nr: " + k);
			matrix[hWall][k] = "V";
		}


		int s = 1;
		int count = 0;
		for (int i= 0; i < hWall; i++ ) {
			if (matrix[i][vWall+1].equals(" ") && matrix[i+1][vWall+1].equals(" ") && matrix[i+2][vWall+1].equals(" ") &&
				matrix[i][vWall-1].equals(" ") && matrix[i+1][vWall-1].equals(" ") && matrix[i+2][vWall-1].equals(" ")) {
				count++;
			}
		}
		if (count == 0) System.out.println("BAJSBAJSBAJBSJABSJABS      1");
		int r = rand.nextInt(count);
		
		for (int i = 1; i < hWall; i++ ) {
			if (matrix[i][vWall+1].equals(" ") && matrix[i+1][vWall+1].equals(" ") && matrix[i+2][vWall+1].equals(" ") &&
				matrix[i][vWall-1].equals(" ") && matrix[i+1][vWall-1].equals(" ") && matrix[i+2][vWall-1].equals(" ")) {
				if (r <= 0) {
					s = i + 1;
					matrix[s + 1][vWall] =  " ";
					matrix[s + 0][vWall] = "P";
					matrix[s - 1][vWall] = " ";
					
					i = hWall;
				}
				r--;
			}
		}
		
//		if (hWall > 4) {
//			s = 1 + rand.nextInt(hWall-4);
//		}
//
//		for (int i = s; i < 3+s; i++) {
//			matrix[i][vWall] = " ";	
//		}

		s = 1;
		count = 0;
		for (int i = hWall; i < matrix.length - 2; i++ ) {
			if (matrix[i][vWall+1].equals(" ") && matrix[i+1][vWall+1].equals(" ") && matrix[i+2][vWall+1].equals(" ") &&
				matrix[i][vWall-1].equals(" ") && matrix[i+1][vWall-1].equals(" ") && matrix[i+2][vWall-1].equals(" ")) {
				count++;
			}
		}
		if (count == 0) System.out.println("BAJSBAJSBAJBSJABSJABS     2");
		r = rand.nextInt(count);
		
		for (int i = hWall; i < matrix.length - 2; i++ ) {
			if (matrix[i][vWall+1].equals(" ") && matrix[i+1][vWall+1].equals(" ") && matrix[i+2][vWall+1].equals(" ") &&
				matrix[i][vWall-1].equals(" ") && matrix[i+1][vWall-1].equals(" ") && matrix[i+2][vWall-1].equals(" ")) {
				if (r <= 0) {
					s = i + 1;
					matrix[s + 1][vWall] = " ";
					matrix[s + 0][vWall] = "P";
					matrix[s - 1][vWall] = " ";
					
					i = matrix.length;
				}
				r--;
			}
		}
			
//		s = hWall + 1;
//
//		if (matrix.length - s - 3 > 0) {
//			s = s + rand.nextInt(matrix.length - s - 3);
//		}
//
//		for (int i = s; i < 3 + s; i++) {
//			matrix[i][vWall] = " ";
//		}

		s = 1;
		count = 0;
		for (int k= 1; k < vWall; k++ ) {
			if (matrix[hWall+1][k].equals(" ") && matrix[hWall+1][k+1].equals(" ") && matrix[hWall+1][k+2].equals(" ") &&
				matrix[hWall-1][k].equals(" ") && matrix[hWall-1][k+1].equals(" ") && matrix[hWall-1][k+2].equals(" ")) {
				count++;
			}
		}
		if (count == 0) System.out.println("BAJSBAJSBAJBSJABSJABS       3");
		r = rand.nextInt(count);
		
		for (int k = 1; k < vWall; k++ ) {
			if (matrix[hWall+1][k].equals(" ") && matrix[hWall+1][k+1].equals(" ") && matrix[hWall+1][k+2].equals(" ") &&
					matrix[hWall-1][k].equals(" ") && matrix[hWall-1][k+1].equals(" ") && matrix[hWall-1][k+2].equals(" ")) {
				if (r <= 0) {
					s = k + 1;
					matrix[hWall][s + 1] = " ";
					matrix[hWall][s + 0] = "P";
					matrix[hWall][s - 1] = " ";
					
					k = vWall;
				}
				r--;
				
			}
		}
		
		
//		##############
//		s = 1;
//		if (vWall > 4) {
//			s = 1 + rand.nextInt(vWall-4);
//		}
//
//		for (int i = s; i < 3+s; i++) {
//			matrix[hWall][i] = " ";
//		}
		s = 1;
		count = 0;
		for (int k = vWall; k < matrix[0].length-2; k++) {
			if (matrix[hWall+1][k].equals(" ") && matrix[hWall+1][k+1].equals(" ") && matrix[hWall+1][k+2].equals(" ") &&
				matrix[hWall-1][k].equals(" ") && matrix[hWall-1][k+1].equals(" ") && matrix[hWall-1][k+2].equals(" ")) {
				count++;
			}
		}
		if (count == 0) System.out.println("BAJSBAJSBAJBSJABSJABS      4");
		r = rand.nextInt(count);
		
		for (int k = vWall; k < matrix[0].length-2; k++) {
			if (matrix[hWall+1][k].equals(" ") && matrix[hWall+1][k+1].equals(" ") && matrix[hWall+1][k+2].equals(" ") &&
					matrix[hWall-1][k].equals(" ") && matrix[hWall-1][k+1].equals(" ") && matrix[hWall-1][k+2].equals(" ")) {
				if (r <= 0) {
					s = k + 1;
					matrix[hWall][s + 1] = " ";
					matrix[hWall][s + 0] = "P";
					matrix[hWall][s - 1] = " ";
					
					k = matrix[0].length;
					
				}
				r--;
			}
		}
//		c = s;
//
//		s = vWall + 1;
//		for (int i = s; i < 3+s; i++) {
//			matrix[hWall][i] = " ";
//		}

		matrix[hWall][vWall] = "X";

		return matrix;
	}


	public void loadObject(String[][] map){
		int wallwidth = 0;
		for (int i = 0; i < map.length; i++){
			for (int k = 0; k < map[0].length; k++){
				//System.out.print(".");
				if(map[i][k].equals("V") || map[i][k].equals("X")){
					//System.out.println("loading");
					wallwidth++;
				}
				if (wallwidth > 0 && ((!map[i][k].equals("V") && !map[i][k].equals("X")) || k == map[0].length-1)){
					//TankyObjectWall wall = new TankyObjectWall(new Vector3f(2*i-width, 0f, k-height), mainNode, assetManager, wallwidth, true);
					Wall wall = new Wall(new Vector3f(2*i-width, 0f, 2*k-height-wallwidth), mainNode, assetManager, wallwidth, true);

					wall.initTankyObject();
					objList.add(wall);
					wallwidth = 0;
					//System.out.println("wall loaded");
				}
			}
		}
		wallwidth = 0;
		for (int k = 0; k < map[0].length; k++){
			for (int i = 0; i < map.length; i++){
				if(map[i][k].equals("H") || map[i][k].equals("X")){
					wallwidth++;
				}
				if (wallwidth > 0 && ((!map[i][k].equals("H") && !map[i][k].equals("X")) || i == map.length-1)){
					//TankyObjectWall wall = new TankyObjectWall(new Vector3f(i-width, 0f, 2*k-height), mainNode, assetManager, wallwidth, false);
					Wall wall = new Wall(new Vector3f(2*i-width-wallwidth, 0f, 2*k-height), mainNode, assetManager, wallwidth, false);
					wall.initTankyObject();
					objList.add(wall);
					wallwidth = 0;
					//System.out.println("wall loaded");
				}
			}
		}
	}

	public void loadSpawnNodes(String[][] map) {
		for (int i = 0; i < map.length; i++){
			for (int k = 0; k < map[0].length; k++){
				if(map[i][k].equals("P") || map[i][k].equals("S")) {
					Node n = new Node();

					n.move((i*2 - map.length) + 0.1f, 0, k*2 - map[0].length);
					spawnNodes.add(n);
					mainNode.attachChild(n);
				}

				if(map[i][k].equals("P")) {
					Node n = new Node();
					n.move(i*2 - map.length, 1, k*2 - map[0].length);
					PointOfInterest poi = new PointOfInterest(n.getWorldTranslation(), rootNode, assetManager);
					navPoints.add(poi); 
				}
			}
		}
	}

	public void loadLights(float x, float y) {
		float dist = (float) Math.hypot(x, y) * 1.8f;

		PointLight lampR = new PointLight();
		lampR.setColor(ColorRGBA.Red);
		lampR.setRadius(dist);
		lampR.setPosition(new Vector3f(x, (x+y)/2, y));
		rootNode.addLight(lampR);

		PointLight lampB = new PointLight();
		lampB.setColor(ColorRGBA.Blue);
		lampB.setRadius(dist);
		lampB.setPosition(new Vector3f(-x, (x+y)/2, y));
		rootNode.addLight(lampB);

		PointLight lampP = new PointLight();
		lampP.setColor(ColorRGBA.Magenta);
		lampP.setRadius(dist);
		lampP.setPosition(new Vector3f(-x, (x+y)/2, -y));
		rootNode.addLight(lampP);

		PointLight lampY = new PointLight();
		lampY.setColor(ColorRGBA.Yellow);
		lampY.setRadius(dist);
		lampY.setPosition(new Vector3f(x, (x+y)/2, -y));
		rootNode.addLight(lampY);
	}

	public ArrayList<GameObject> getObjects(){
		return objList;
	}

	public void initTankyWorld() {
		rootNode.attachChild(mainNode);
	}

	public boolean containsNode(Node node){
		for(int i = 0; i < objList.size(); i++) {
			if (objList.get(i).containsNode(node)) {
				return true;
			}
		}
		return false;
	}
	public void bounce(Node node){
		for(int i = 0; i < objList.size(); i++) {
			if (objList.get(i).containsNode(node)) {
				objList.get(i).bounce(node);
			}
		}
	}

	public String[][] getMap(){
		return map;
	}

	@Override
	public String toString(){
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < map.length; i++) {
			for (int k = 0; k < map[0].length; k++){
				b.append("]" + map[i][k] + "[");
			}
			b.append("\n");
		}
		b.reverse();
		return b.toString();
	}

	public ArrayList<Node> getSpawnNodes() {
		return spawnNodes;
	}

	public ArrayList<PointOfInterest> getPoi() {
		return navPoints;
	}
}
