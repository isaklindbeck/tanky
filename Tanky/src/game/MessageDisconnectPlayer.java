package game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessageDisconnectPlayer extends AbstractMessage {
	public String name;

	public MessageDisconnectPlayer(){
	}
	
	public MessageDisconnectPlayer(String name){
		this.name = name;
	}
}
