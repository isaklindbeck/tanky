package game;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class MessagePlayerState extends AbstractMessage{
	public PlayerState state;

	public MessagePlayerState(){
	}
	
	public MessagePlayerState(PlayerState state){
		this.state = state;
	}
}
