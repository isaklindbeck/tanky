package game;

import java.util.ArrayList;

import com.jme3.app.Application;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;

public class PlayerHandler {
	private Node rootNode;
	private Camera cam;
	private BulletHandler bulletHandler;
	private ArrayList<String> playerNameList;
	ArrayList<Player> list;
	ArrayList<Node> spawnNodes;
	private boolean scoreChange = false;
	private Application app;
	private ArrayList<PointOfInterest> poiList;
	private ArrayList<PlayerController> controllerList;
	private Node POINode;
	
	public PlayerHandler(Node rootNode, World world, ArrayList<Node> spawnNodes,ArrayList<PointOfInterest> poiList, Application app, Camera cam, BulletHandler bulletHandler) {
		this.app = app;
		list = new ArrayList<Player>();
		playerNameList = new ArrayList<String>();
		this.rootNode = rootNode;
		this.cam = cam;
		this.bulletHandler = bulletHandler;
		this.spawnNodes = spawnNodes;
		this.poiList = poiList;
		this.controllerList = new ArrayList<PlayerController>();
		POINode = new Node();

		if (poiList != null) {
			for (int i = 0; i < poiList.size(); i++) {
				POINode.attachChild(poiList.get(i).getMainNode());
			}
			rootNode.attachChild(POINode);
			for (int i = 0; i < poiList.size(); i++) {
				poiList.get(i).lookForNeighbours(rootNode, poiList);
			}
		}		
	}
	
	public void addPlayer(Vector3f pos, String name) {
		Player newPlayer = new Player(pos, rootNode, spawnNodes, app, cam, name, bulletHandler, true);
		list.add(newPlayer);
		newPlayer.initTankyChar();
	}
	
	public void addPlayer(Player player) {
		list.add(player);
		player.initTankyChar();
		playerNameList.add(player.name);
	}
	
	public void addComputerControlledPlayer() {
		String name;
		if(controllerList.size() < 10) {
			name = "NPC0" + controllerList.size();
		} else {
			name = "NPC" + controllerList.size();
		}
		Player newPlayer = new Player(new Vector3f(0,1,0), rootNode, spawnNodes, app, cam, name, bulletHandler, true);
		list.add(newPlayer);
		newPlayer.initTankyChar();
		PlayerController ctrlr = new PlayerController(newPlayer, rootNode, list, poiList);
		controllerList.add(ctrlr);
		
	}
	
	public void updateState(float tpf) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).updateState(tpf);
		}
	}
	
	public void setAction(ArrayList<MessagePlayerControll> actionList, float tpf) {
		for (int i = 0; i < actionList.size(); i++) {
			int index = list.indexOf(actionList.get(i));
			if (index >= 0) {
				list.get(index).setAction(actionList.get(i));
			}
		}
		for (int i = 0; i < controllerList.size(); i++) {
			controllerList.get(i).applyAction(tpf);
		}
	}
	
	public void playerCheck(ArrayList<String> nameList) {
		if (!playerNameList.containsAll(nameList)) {
			for (int i = 0; i < nameList.size(); i++) {
				if (!playerNameList.contains(nameList.get(i))) {
					System.out.println("adding playa" + nameList.get(i));
					playerNameList.add(nameList.get(i));
					addPlayer(new Vector3f(0,1,0), nameList.get(i));
				}
			}
		}
		if (!nameList.containsAll(playerNameList)) {
			System.out.println("should only happen on d/c");
			for (int i = 0; i < playerNameList.size(); i++) {
				if (!nameList.contains(playerNameList.get(i))) {
					System.out.println("1 happens");
					playerNameList.remove(i);
					i--;
				}
			}
			for (int i = 0; i < list.size(); i++) {
				if (!nameList.contains(list.get(i).name)) {
					System.out.println("player disconnected");
					list.get(i).mainNode.removeFromParent();
					list.remove(i);
					i--;
				}
			}
		}
		
	}
	
	public void setStates(ArrayList<PlayerState> stateList) {
		if (stateList == null) return;
		
		for (int i = 0; i < stateList.size(); i++) {
			int index = list.indexOf(stateList.get(i));
			if (index >= 0) {
				list.get(index).setState(stateList.get(i));
			}
		}
	}
	
	public ArrayList<PlayerState> getStates() {
		ArrayList<PlayerState> stateList = new ArrayList<PlayerState>();
		for (int i = 0; i < list.size(); i++) {
			stateList.add(list.get(i).getState());
		}
		return stateList;
	}
	
	public void colissionDetect(World world) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).colissionDetect(world);
		}
	}
	
	public ArrayList<Player> getPlayerList() {
		return list;
	}
	
	public int getScore(String player) {
		int score = -1;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(player)) {
				score = list.get(i).score;
			}
		}
		return score;
	}
	
	public void reset(ArrayList<Node> spawnNodes, ArrayList<PointOfInterest> pl) {
		rootNode.detachChild(POINode);
//		POINode.detachChild(worldNode);
		POINode = new Node();
//		POINode.attachChild(worldNode);
		poiList = pl;
		if (poiList != null) {
			for (int i = 0; i < poiList.size(); i++) {
				POINode.attachChild(poiList.get(i).getMainNode());
			}
		}
		rootNode.attachChild(POINode);
		
		if (poiList != null) {
			for (int i = 0; i < poiList.size(); i++) {
				poiList.get(i).lookForNeighbours(rootNode, poiList);
			}
		}
		for (int i = 0; i < list.size(); i++) {
			list.get(i).score = 0;
			list.get(i).lvl = 1;
			list.get(i).lvlcounter = 0;
			list.get(i).dmg = 5;
			list.get(i).fullHealth = 20;
			list.get(i).currentHealth = 20;
			list.get(i).cooldown = 1.5f;
			list.get(i).bounces = 1;
			list.get(i).spawnNodes = spawnNodes;
			list.get(i).dead = true;
			list.get(i).pointsToLvl = 1;
		}
		
		for (int i = 0; i < controllerList.size(); i++) {
			controllerList.set(i, new PlayerController(controllerList.get(i).player, rootNode, list, poiList));
		}
	}
	
	public boolean hasScoreChanged() {
		return scoreChange;
	}
}
