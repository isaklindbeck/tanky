package game;

import com.jme3.app.Application;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

public class Bullet {
	private Node mainNode;
	private Node rootNode;
	private AssetManager assetManager;
	private Application app;
	private boolean inside = false;
	private boolean insidePlayer = true;
	private int bounces;
	public boolean alive = true;
	public String owner;
	public float dmg;
	public int id;
	
	private AudioNode anStutts;
	private AudioNode anPang;
	
	public Bullet(int id, Vector3f pos, Quaternion direction, Node rootNode, Application app, String owner, int bounces, float dmg){
		this.id = id;
		this.app = app;
		mainNode = new Node();
		mainNode.rotate(direction);
		mainNode.move(pos);
		mainNode.move(mainNode.getLocalRotation().getRotationColumn(2).normalize().mult(-1f));
		this.bounces = bounces;
		this.dmg = dmg;
		this.rootNode = rootNode;
		this.assetManager = app.getAssetManager();
		this.owner = owner;
		initModel();
		initTankyBullet();
		
        anStutts = new AudioNode(assetManager, "sounds/bounce.wav", false);
        anStutts.setLooping(false);
        anStutts.setVolume(0.5f);        
        
        anPang = new AudioNode(assetManager, "sounds/shoot.wav", false);
        anPang.setLooping(false);
        anPang.setMaxDistance(5f);
        anPang.setVolume(0.8f);
        mainNode.attachChild(anStutts);
        mainNode.attachChild(anPang);
        
        
        anPang.setLocalTranslation(mainNode.getLocalTranslation());
        
       if(app.getAudioRenderer() != null) anPang.playInstance();

	}
	
	private void initModel(){
		Material colorMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		colorMat.setColor("m_Color", ColorRGBA.White);
	    colorMat.setColor("GlowColor", ColorRGBA.White);		
		
		Box c = new Box(new Vector3f(0,0,0), 0.1f, 0.1f, 0.5f);
        Geometry cgeom = new Geometry("Box", c);
        cgeom.setMaterial(colorMat);
        
        mainNode.attachChild(cgeom);
	}
	
	public void initTankyBullet(){
		rootNode.attachChild(mainNode);
	}
	
	public Node getNode(){
		return mainNode;
	}
	
	public boolean colissionDetect(World world){
		boolean contains = world.containsNode(mainNode);
		if (contains && !inside){
			playStutts();
			world.bounce(mainNode);
			decreaseBounce();
		}
		inside = contains;
		return bounces <= 0;
	}
	
	public void kill(){
		mainNode.detachAllChildren();
		mainNode.removeFromParent();
		alive = false;
	}
	
	public boolean colissionDetect(Player player){
		boolean contains = player.containsNode(mainNode);
		if (contains && !insidePlayer){
			return true;
		}
		if (player.equals(owner)) {
			insidePlayer = contains;
		}
		return false;
	}
	
	public void decreaseBounce(){
		bounces--;
		if (bounces <= 0) {
			this.kill();
		}
	}
	
	public BulletState getState() {
		return new BulletState(id, mainNode.getWorldRotation(), mainNode.getWorldTranslation(), bounces, dmg);
	}

	public void updateState(float tpf){
		mainNode.move(mainNode.getLocalRotation().getRotationColumn(2).normalize().mult(-tpf*20));
	}
	
	public void setState(BulletState state) {
		mainNode.setLocalRotation(state.rot);
		mainNode.setLocalTranslation(state.pos);
		if (bounces != state.bounces) playStutts();
		bounces = state.bounces;
		dmg = state.dmg;
	}
	
	public void playStutts() {
		if (app.getAudioRenderer() != null) {
			anStutts.setLocalTranslation(mainNode.getWorldTranslation());
			anStutts.playInstance();
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BulletState) return ((BulletState) obj).id == id;
		if (obj instanceof Bullet) return ((Bullet) obj).id == id;
		return false;
	}
}
