package game;

import java.util.ArrayList;
import java.util.Random;

import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;

public class PlayerController {
	public Player player;
	public boolean hasTarget = false;
	private boolean navigating = true;
	public boolean hasDestination = false;
	private Player target = null;
	private PointOfInterest dest = null;
	private PointOfInterest lastVissited = null;
	private ArrayList<Player> playerList;
	private ArrayList<PointOfInterest> poiList;
	private Node rootNode;
	private float lastDist ;
	private float count = 0;
	private float playerCheckCounter;
	
	public PlayerController(Player player, Node rootNode, ArrayList<Player> playerList, ArrayList<PointOfInterest> poiList) {
		this.player = player;
		this.playerList = playerList;
		this.poiList = poiList;
		this.rootNode = rootNode;
		this.playerCheckCounter = 0;
	}
	
	public void applyAction(float tpf) {
//		System.out.println("applying action");
		boolean forward = false;
		boolean backward = false;
		boolean left = false;
		boolean right = false;
		boolean shoot = false;
		
		if (navigating && !hasTarget) {
			if (!hasDestination) {
				ArrayList<PointOfInterest> possibleDest = new ArrayList<PointOfInterest>();
				for (int i = 0; i < poiList.size(); i++) {
					Vector3f source = player.getMainNode().getWorldTranslation().clone();
					Vector3f pos = poiList.get(i).getMainNode().getWorldTranslation().clone();
					Vector3f dir = pos.subtract(source);
					Ray ray = new Ray(source, dir.normalize());

					CollisionResults results = new CollisionResults();
					rootNode.collideWith(ray, results);

					
					for (int k = 0; k < results.size(); k++) {
						String target = results.getCollision(k).getGeometry().getName();
						Geometry geo = results.getCollision(k).getGeometry();

						if (target.equals("wall")) {
							k = results.size();
						} else if (target.equals("POI") && geo == poiList.get(i).geo) {
							possibleDest.add(poiList.get(i));
						}
					}
				}
				
				if (possibleDest.size() > 0) {
					Random rand = new Random();
					int i = rand.nextInt(possibleDest.size());
					dest = possibleDest.get(i);
					hasDestination = true;
				}

			}
			
			if (hasDestination) {
				Vector3f dir = player.getMainNode().getLocalRotation().getRotationColumn(2).normalize();  // q1.getRotationColumn(0);
				Vector3f pos = player.getMainNode().getWorldTranslation().clone();
				Vector3f dest2 = dest.getMainNode().getWorldTranslation().clone().subtract(pos).normalize();

				float fdir = (float) ((Math.atan2(dir.getX(), dir.getZ()) + 2*Math.PI) % (Math.PI*2));
				float fdest = (float)((Math.atan2(dest2.getX(), dest2.getZ()) + 1*Math.PI) % (Math.PI*2));
				fdir = (float) (fdir / Math.PI);
				fdest = (float) (fdest / Math.PI);

				if ( ((0 < (fdest - fdir)) && (1 > (fdest - fdir))) || ((-2 < (fdest - fdir)) && (-1 > (fdest - fdir)))){
					left = true;
					right = false;
				}
				else {
					left = false;
					right = true;
				}
				
				if (Math.abs(fdir - fdest) < 0.06) {
					forward = true;
				} else {
					forward = false;
				}
				
				float distance = player.getMainNode().getWorldTranslation().distance(dest.getMainNode().getWorldTranslation());
				
				if (distance < 0.4) {
					hasDestination = false;
					ArrayList<PointOfInterest> list = dest.getNeighbours();
					for (int i = 0; i < list.size(); i++) {
						if (list.get(i).equals(lastVissited)) {
							list.remove(i);
							i--;
						}
					}
					if (list.size() > 0) {
						Random rand = new Random();
						int p = rand.nextInt(list.size());
						lastVissited = dest;
						dest = list.get(p);
					} else {
						PointOfInterest temp = dest;
						dest = lastVissited;
						lastVissited = temp;
					}
					hasDestination = true;
				}
				
				if (lastDist <= distance) {
					count += tpf;
					if (count >= 10) {
						hasDestination=false;
						System.out.println("stuck, finding new dest");
						count = 0;
					}
				} else {
					count = 0;
				}

				lastDist = distance;
			}
		}
		
//		System.out.println(playerCheckCounter);
		if (!hasTarget && playerCheckCounter >= 0.05) {
			for (int i = 0; i < playerList.size(); i++) {
				if (!playerList.get(i).equals(player)) {
					Vector3f source = player.getMainNode().getWorldTranslation().clone();
					Vector3f pos = playerList.get(i).getMainNode().getWorldTranslation().clone();
					Vector3f dir = pos.subtract(source);
					Ray ray = new Ray(source, dir);
					CollisionResults results = new CollisionResults();
					rootNode.collideWith(ray, results);
					
					for (int k = 0; k < results.size(); k++) {
//						if (player.name.equals("NPC1")) {
//							System.out.println("k: " + k + " name: " + results.getCollision(k).getGeometry().getName());
//						}
						if (results.getCollision(k).getGeometry().getName().equals("wall")) {
							k = results.size();
						} else if (results.getCollision(k).getGeometry().getParent().getParent() == playerList.get(i).cgeom) {
//							System.out.println("found target");
							navigating = false;
							hasTarget = true;
							target = playerList.get(i);
							k = results.size();
							i = playerList.size();
						}
					}
					playerCheckCounter = 0;
				}
			}
		} else if (!hasTarget) {
			playerCheckCounter += tpf;
		}
		
		if (hasTarget) {
			Vector3f source = player.getMainNode().getWorldTranslation().clone();
			Vector3f pos = target.getMainNode().getWorldTranslation().clone();
			Vector3f dir = pos.subtract(source);
			Ray ray = new Ray(source, dir);
			CollisionResults results = new CollisionResults();
			rootNode.collideWith(ray, results);
			
			for (int k = 0; k < results.size(); k++) {
//				System.out.println(results.getCollision(k).getGeometry().getName());
				if (results.getCollision(k).getGeometry().getName().equals("wall") || target.mainNode.getWorldTranslation().y != 1) {
//					System.out.println("lost target");
					hasTarget = false;
					navigating = true;
					target = null;
					k = results.size();
				} else if (results.getCollision(k).getGeometry().getName().equals("Sphere1")){
					if (results.getCollision(k).getGeometry().getParent().getParent() == target.cgeom) {
						k = results.size();
					}
				}
			}
			
			if (hasTarget) {
				Vector3f dir2 = player.getMainNode().getLocalRotation()
						.getRotationColumn(2).normalize(); // q1.getRotationColumn(0);
				Vector3f pos2 = player.getMainNode().getWorldTranslation().clone();
				Vector3f dest3 = target.getMainNode().getWorldTranslation().clone().subtract(pos2).normalize();
				float fdir = (float) ((Math.atan2(dir2.getX(), dir2.getZ()) + 2 * Math.PI) % (Math.PI * 2));
				float fdest = (float) ((Math.atan2(dest3.getX(), dest3.getZ()) + 1 * Math.PI) % (Math.PI * 2));
				fdir = (float) (fdir / Math.PI);
				fdest = (float) (fdest / Math.PI);
				if (((0 < (fdest - fdir)) && (1 > (fdest - fdir)))
						|| ((-2 < (fdest - fdir)) && (-1 > (fdest - fdir)))) {
					left = true;
					right = false;
				} else {
					left = false;
					right = true;
				}
				if (Math.abs(fdir - fdest) < 0.06) {
					shoot = true;
				} else {
					shoot = false;
				}
			}
		}
		
		int lvlupChoice = 0;
		if (player.lvlcounter > 0) {
			Random rand = new Random();
			lvlupChoice = rand.nextInt(4) + 1;
		}
		
		MessagePlayerControll ctrl = new MessagePlayerControll(null, 0, forward, backward, left, right, shoot, lvlupChoice);
		player.setAction(ctrl);
	}
	

}
