package game;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.Environment;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.LightList;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.system.AppSettings;
import com.jme3.util.SkyFactory;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.controls.TextFieldChangedEvent;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.TextRenderer;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class AAMain extends SimpleApplication implements ScreenController, ActionListener {
	private Player player;
	private World world;
	private BulletHandler bulletHandler; 
	private GameServer server;
	private GameClient client;
	private boolean loaded = false;
	private boolean started = false;
	private boolean isServer;
	private float serverClock = 0;
	private float clientClock = 0;
	private PlayerHandler playerHandler;
	private Nifty nifty;
	private String playerName = "default";
	private Element popup;
	private int scoreGoal;
	private static boolean opengl1 = false;
	private static boolean dedicatedServer = false;
	private boolean popupIsVisible = false;
	private static boolean statTest = false;

	//TODO namn. detahSpehres, warballz, battleballs
	//TODO dedServer settings.

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		AAMain app = new AAMain();
		app.setPauseOnLostFocus(false);
		AppSettings apps = new AppSettings(true);
		
        for (String s: args) {
        	System.out.println(s);
            if (s.equals("opengl1")) {
            	apps.setRenderer(AppSettings.LWJGL_OPENGL1);
            	opengl1 = true;
            }
            if (s.equals("server")) {
            	dedicatedServer = true;
            	apps.setRenderer(AppSettings.NULL);
        	    apps.setAudioRenderer(null);
            }
            if (s.equals("statTest")) {
            	statTest = true;
            }
        }
        
        apps.setFrameRate(60);
		app.setSettings(apps);
		app.start();
	}

	public void simpleInitApp() {
		Logger.getLogger("").setLevel(Level.SEVERE);
		this.setDisplayFps(true);
		this.setDisplayStatView(false);
		this.setShowSettings(false);
		flyCam.setEnabled(false);
		this.getInputManager().deleteMapping( SimpleApplication.INPUT_MAPPING_EXIT );
		initGui();
		initGuiKeys();
		
		if (!dedicatedServer) {
			audioRenderer.setEnvironment(new Environment(Environment.Closet));
		}
		cam.setFrustumNear(0.5f);

	    FilterPostProcessor fpp;
	    if (!opengl1) {
			fpp = new FilterPostProcessor(assetManager);
			BloomFilter bloom = new BloomFilter(BloomFilter.GlowMode.Objects);
			fpp.addFilter(bloom);
			viewPort.addProcessor(fpp);
		}
	    if (dedicatedServer) {
	    	startDedicatedServer();
	    }
		
	}
	
	public void startDedicatedServer() {
		scoreGoal = 5;
		world = new World(rootNode, assetManager, 40, 40);
		world.initTankyWorld();
		server = new GameServer(4001, "gameON!", world.getMap(), null, scoreGoal, 16);
		loaded = true;
		bulletHandler = new BulletHandler(rootNode, this);
		playerHandler = new PlayerHandler(rootNode, world, world.getSpawnNodes(), world.getPoi(), this, cam, bulletHandler);
		started = true;
	}
	
	public void startGame(String is) {
		isServer = false;
		if (is.equals("true")) {
			isServer = true;
		}
		
		if (isServer) {
			int port = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("port", TextField.class)).getText());

			int height = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("height", TextField.class)).getText());
			int width = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("width", TextField.class)).getText());
			
			scoreGoal = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("score", TextField.class)).getText());
			int nbrPlayers = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("players", TextField.class)).getText());
			nifty.getScreen("gameOverlay").findElementByName("layer3").findElementByName("panel").findElementByName("text").getRenderer(TextRenderer.class).setText("First to  " + scoreGoal + "  points wins!");
			world = new World(rootNode, assetManager, width, height);
			world.initTankyWorld();
			System.out.println(world.toString());
			server = new GameServer(port, "gameON!", world.getMap(), playerName, scoreGoal, nbrPlayers);
			loaded = true;
		} else {
			String ip = ((TextField) nifty.getScreen("start").findNiftyControl("ip", TextField.class)).getText();
			int port = Integer.parseInt(((TextField) nifty.getScreen("start").findNiftyControl("port", TextField.class)).getText());
			try {
				client = new GameClient(port, ip, playerName);
			} catch (Exception e) {
				showPopup("Could not conect to server.");
				e.printStackTrace();
				return;
			}
		}
		
		bulletHandler = new BulletHandler(rootNode, this);
		if (isServer) {
			playerHandler = new PlayerHandler(rootNode, world, world.getSpawnNodes(), world.getPoi(), this, cam, bulletHandler);
			player = new Player(new Vector3f(0, 1, 0), rootNode,world.getSpawnNodes(), this, cam, playerName, bulletHandler, false);
			nifty.gotoScreen("gameOverlay");
			int nbrNpc = Integer.parseInt(((TextField) nifty.getScreen("serverSettigns").findNiftyControl("computers", TextField.class)).getText());
			for (int n = 0; n < nbrNpc; n++) {
				playerHandler.addComputerControlledPlayer();
			}	

		} else {
			playerHandler = new PlayerHandler(rootNode, world, null,null, this, cam, bulletHandler);
			player = new Player(new Vector3f(0, 1, 0), rootNode, null, this, cam, playerName, bulletHandler, false);
		}
		playerHandler.addPlayer(player);
		player.updateCam();
		initKeys();
		rootNode.attachChild(SkyFactory.createSky(assetManager, "textures/skybox.jpg", true));
		started = true;
	}

	private void setWorld() {
		if (client.getMap() != null) {
			world = new World(rootNode, assetManager, client.getMap());
			world.initTankyWorld();
			scoreGoal = client.getScoreGoal();
			nifty.getScreen("gameOverlay").findElementByName("layer3").findElementByName("panel").findElementByName("text").getRenderer(TextRenderer.class).setText("First to  " + scoreGoal + "  points wins!");
			loaded = true;
			nifty.gotoScreen("gameOverlay");
		}
	}

	private void initKeys() {
		inputManager.addMapping("forward", new KeyTrigger(KeyInput.KEY_UP));
		inputManager.addMapping("backpedal", new KeyTrigger(KeyInput.KEY_DOWN));
		inputManager.addMapping("turnleft", new KeyTrigger(KeyInput.KEY_LEFT));
		inputManager.addMapping("turnright", new KeyTrigger(KeyInput.KEY_RIGHT));
		inputManager.addMapping("shoot", new KeyTrigger(KeyInput.KEY_SPACE));
		inputManager.addMapping("1", new KeyTrigger(KeyInput.KEY_1));
		inputManager.addMapping("2", new KeyTrigger(KeyInput.KEY_2));
		inputManager.addMapping("3", new KeyTrigger(KeyInput.KEY_3));
		inputManager.addMapping("4", new KeyTrigger(KeyInput.KEY_4));
		inputManager.addMapping("5", new KeyTrigger(KeyInput.KEY_9));
		
//		inputManager.addMapping("forward", new JoyAxisTrigger(0, 2, true));
//		inputManager.addMapping("backpedal", new JoyAxisTrigger(0, 2, false));
//		inputManager.addMapping("turnleft", new JoyAxisTrigger(0, 3, false));
//		inputManager.addMapping("turnright", new JoyAxisTrigger(0, 3, true));
//		
//		inputManager.addMapping("shoot", new JoyButtonTrigger(0,0));
		
		inputManager.addListener(player, new String[] { "forward", "backpedal", "turnleft", "turnright", "shoot", "1", "2", "3", "4", "5" });
	}
	
	private void initGuiKeys() {
		inputManager.addMapping("menu", new KeyTrigger(KeyInput.KEY_ESCAPE));
		inputManager.addMapping("okError", new KeyTrigger(KeyInput.KEY_RETURN));
		inputManager.addListener(this, new String[] {"menu", "okError"});
	}

	public void simpleUpdate(float tpf) {
		if (loaded && started) {
			if (isServer) {
				serverUpdate(tpf);
			} else if (dedicatedServer) { 
				dedicatedServerUpdate(tpf);
			} else {
				clientUpdate(tpf);
			}
		} else if(started) {
			setWorld();
		}
	}

	public void serverUpdate(float tpf) {
		//Spola
		tpf = tpf * 1;
		bulletHandler.updateState(tpf);
		collision();
		playerHandler.updateState(tpf);
		listener.setLocation(cam.getLocation());
	    listener.setRotation(cam.getRotation());
	    //TODO change player cam here.
		player.updateCam();
//		cam.setLocation(new Vector3f(player.getMainNode().getWorldTranslation()).add(0, 80, 0));
//		cam.setLocation(new Vector3f(0,50,0));
//		cam.lookAtDirection(new Vector3f(0,-1000,0), new Vector3f(1,0,0));
		
		if (serverClock < 0.015) {
			serverClock += tpf;
			return;
		}		
		
		updateScoreBoard();
		updatplayerStatusHud();
		//Read incoming states.
		ArrayList<MessagePlayerControll> ctrlList = server.getControlList();
		playerHandler.setAction(ctrlList, tpf);
		
		//Send states to clients.
		server.brodcastState(playerHandler.getStates(), bulletHandler.getStates());
		
		//Check for new players.
		ArrayList<String> list = server.getPlayerList();
		list.add(playerName);
		playerHandler.playerCheck(list);
		ArrayList<Player> plist = playerHandler.getPlayerList();
		for (int i = 0; i < plist.size(); i++) {
			if (playerHandler.getScore(plist.get(i).name) >= scoreGoal) {
				LightList lightList = rootNode.getWorldLightList();
				for (int k = 0; k < lightList.size(); k++) {
					rootNode.removeLight(lightList.get(k));
				}
				rootNode.detachChild(world.mainNode);
				int height = world.getMap().length;
				int width  = world.getMap()[0].length;
				world = new World(rootNode, assetManager, width, height);
				world.initTankyWorld();
				System.out.println(world.toString());
				playerHandler.reset(world.getSpawnNodes(), world.getPoi());
				server.setMap(world.getMap());
				server.brodcastMap();
			}
		}
		
		serverClock = 0;
	}
	
	public void dedicatedServerUpdate(float tpf) {
		bulletHandler.updateState(tpf);
		collision();
		playerHandler.updateState(tpf);
		if (serverClock < 0.015) {
			serverClock += tpf;
			return;
		}
		//Read incoming states.
		ArrayList<MessagePlayerControll> ctrlList = server.getControlList();
		playerHandler.setAction(ctrlList, tpf);
		
		//Send states to clients.
		server.brodcastState(playerHandler.getStates(), bulletHandler.getStates());
		
		//Check for new players.
		ArrayList<String> list = server.getPlayerList();
		playerHandler.playerCheck(list);
		ArrayList<Player> plist = playerHandler.getPlayerList();
		for (int i = 0; i < plist.size(); i++) {
			System.out.println(plist.get(i).name);
			if (playerHandler.getScore(plist.get(i).name) >= scoreGoal) {
				LightList lightList = rootNode.getWorldLightList();
				for (int k = 0; k < lightList.size(); k++) {
					rootNode.removeLight(lightList.get(k));
				}
				rootNode.detachChild(world.mainNode);
				int height = world.getMap().length;
				int width  = world.getMap()[0].length;
				world = new World(rootNode, assetManager, width, height);
				playerHandler.reset(world.getSpawnNodes(), world.getPoi());
				world.initTankyWorld();
				server.setMap(world.getMap());
				server.brodcastMap();
				System.out.println(playerHandler.getScore(list.get(i))+" sending map " + scoreGoal);
			}
		}
	}
	
	public void clientUpdate(float tpf) {
		if (clientClock < 0.015) {
			clientClock += tpf;
			return;
		}
		
		//Read and apply incoming states.
		ArrayList<PlayerState> states = client.readState();
		playerHandler.setStates(states);
		bulletHandler.setStates(client.getBulletStates());
		bulletHandler.updateState(tpf);
		
		//Send state to server.
		client.sendControls(player.getControl());
		
		//Check for new players.
		ArrayList<String> list = client.getPlayerList();		
		playerHandler.playerCheck(list);
		
		player.updateCam();
//		cam.setLocation(new Vector3f(player.getMainNode().getWorldTranslation()).add(0, 30, 0));
//		cam.lookAtDirection(new Vector3f(0,-1,0), new Vector3f(0,1,0));

		listener.setLocation(cam.getLocation());
	    listener.setRotation(cam.getRotation());
	    
		updateScoreBoard();
		updatplayerStatusHud();
		if (!client.isConnected()) {
			disconnect();
		}
		
		if (client != null && !client.getMap().equals(world.getMap())) {
			LightList lightList = rootNode.getWorldLightList();
			for (int k = 0; k < lightList.size(); k++) {
				rootNode.removeLight(lightList.get(k));
			}
			rootNode.detachChild(world.mainNode);

			world = new World(rootNode, assetManager, client.getMap());
			playerHandler.reset(world.getSpawnNodes(), world.getPoi());	
			world.initTankyWorld();
		}
		
		clientClock = 0;
		

	}

	public void collision() {
		if (isServer || dedicatedServer) {
			bulletHandler.worldColission(world);
			bulletHandler.playerColission(playerHandler.getPlayerList());
			playerHandler.colissionDetect(world);
		}
	}

	public void initGui() {
		NiftyJmeDisplay nd = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
		nifty = nd.getNifty();
		guiViewPort.addProcessor(nd);
		new ButtonBuilder("appendButton", "Append");
		nifty.fromXml("interface/interface.xml", "start", this);
		guiViewPort.addProcessor(nd);
		Screen screen = nifty.getScreen("start");
		TextField tf = screen.findNiftyControl("apbt", TextField.class);
		tf.setText(playerName);
		tf = screen.findNiftyControl("ip", TextField.class);
		tf.setText("127.0.0.1");
		tf = screen.findNiftyControl("port", TextField.class);
		tf.setText("4001");
		
		Screen gs = nifty.getScreen("gameOverlay");		
		Element layer = gs.findElementByName("layer");		
		Element panel = layer.findElementByName("panel");		
		Element text = panel.findElementByName("score");		
		text.getRenderer(TextRenderer.class).setText("Score:");		
		text = panel.findElementByName("player");		
		text.getRenderer(TextRenderer.class).setText("Player:");
		
		popup = nifty.createPopup("popup");
	}
	
	public void updatplayerStatusHud() {
		Element e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("health");
		int fullWidth = e.getWidth();
		
		float healthStatus = player.currentHealth/player.fullHealth;
		int width = (int)(fullWidth * healthStatus);		
		e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("health").findElementByName("bar");
		e.setWidth(width);		
		String s = (int) player.currentHealth + " / " + (int) player.fullHealth;		
		e.findElementByName("state").getRenderer(TextRenderer.class).setText(s);		
		e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("cooldown");
		fullWidth = e.getWidth();
		float cooldownStatus;		
		if(player.cooldownClock > player.cooldown) {
			cooldownStatus = 1;
		} else {
			cooldownStatus = player.cooldownClock / player.cooldown;
		}
		
		width = (int)(fullWidth * cooldownStatus);		
		e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("cooldown").findElementByName("bar");
		e.setWidth(width);
		
		if (player.lvlcounter > 0) {
			e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("stats");
			e.findElementByName("health").getRenderer(TextRenderer.class).setText(" Health: " + (int)player.fullHealth + ", Press 1 to increase");
			e.findElementByName("dmg").getRenderer(TextRenderer.class).setText(" Damage: " + (int)player.dmg + ", Press 2 to increase");
			e.findElementByName("bounces").getRenderer(TextRenderer.class).setText(" Bounces: " + (player.bounces - 1) + ", Press 3 to increase");
			e.findElementByName("cooldown").getRenderer(TextRenderer.class).setText(" Cooldown: " + Math.round(player.cooldown * 100) / 100f + ", Press 4 to reduce");
		} else {
			e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("stats");
			e.findElementByName("health").getRenderer(TextRenderer.class).setText(" Health: " + (int)player.fullHealth);
			e.findElementByName("dmg").getRenderer(TextRenderer.class).setText(" Damage: " + (int)player.dmg);
			e.findElementByName("bounces").getRenderer(TextRenderer.class).setText(" Bounces: " + (player.bounces - 1));
			e.findElementByName("cooldown").getRenderer(TextRenderer.class).setText(" Cooldown: " + Math.round(player.cooldown * 100) / 100f);
		}
		
		
		//TODO test hud
		if (player.lvlcounter > 0 && statTest) {
			e = nifty.getScreen("gameOverlay").findElementByName("layer2").findElementByName("stats");
			e.findElementByName("health").getRenderer(TextRenderer.class).setText(" Health: " + (int)player.fullHealth + ", Press 1 to increase to " + (player.fullHealth + player.fullHealth/4));
			e.findElementByName("dmg").getRenderer(TextRenderer.class).setText(" Damage: " + (int)player.dmg + ", Press 2 to increase "  + (int)(player.dmg + 2 + player.dmg/10) + ", new dps: " + (player.dmg + 1 + player.dmg/10)/player.cooldown);
			e.findElementByName("bounces").getRenderer(TextRenderer.class).setText(" Bounces: " + (player.bounces - 1) + ", Press 3 to increase by one");
			e.findElementByName("cooldown").getRenderer(TextRenderer.class).setText(" Cooldown: " + Math.round(player.cooldown * 100) / 100f + ", Press 4 to reduce to " + (Math.round(player.cooldown * 100 * 0.875) / 100f) + ", new dps: " + (player.dmg/(Math.round(player.cooldown * 100 * 0.875) / 100f)));
		}

	}
	
	public void updateScoreBoard() {
		TextRenderer textRendererScore = nifty.getScreen("gameOverlay").findElementByName("layer").findElementByName("panel").findElementByName("score").getRenderer(TextRenderer.class);
		TextRenderer textRendererPlayer = nifty.getScreen("gameOverlay").findElementByName("layer").findElementByName("panel").findElementByName("player").getRenderer(TextRenderer.class);
		TextRenderer textRendererLvl = nifty.getScreen("gameOverlay").findElementByName("layer").findElementByName("panel").findElementByName("lvl").getRenderer(TextRenderer.class);

		
		StringBuilder sbLvl = new StringBuilder();
		sbLvl.append("lvl:\n");		
		StringBuilder sbPlayer = new StringBuilder();
		sbPlayer.append("Player:\n");		
		StringBuilder sbScore = new StringBuilder();
		sbScore.append("Score:\n");		
		ArrayList<Player> list = playerHandler.getPlayerList();
		
		for (int i = 0; i < list.size(); i++) {
			sbLvl.append(list.get(i).lvl + "\n");
			sbPlayer.append(list.get(i).name + "\n");
			sbScore.append(playerHandler.getScore(list.get(i).name) + " \n");
		}
		
		textRendererLvl.setText(sbLvl.toString());
		textRendererScore.setText(sbScore.toString());
		textRendererPlayer.setText(sbPlayer.toString());
	}

	@NiftyEventSubscriber(id="apbt")
	public void onButtonEvent(String str, final TextFieldChangedEvent event) {
		playerName=event.getText();
	}

	@Override
	public void bind(Nifty arg0, Screen arg1) {	
	}

	@Override
	public void onEndScreen() {
	}

	@Override
	public void onStartScreen() {	

	}

	@Override
	public void onAction(String action, boolean value, float tpf) {
        if(action.equals("menu") && nifty.getCurrentScreen().equals(nifty.getScreen("gameOverlay"))) {
        	if (value) {
        		nifty.gotoScreen("menu");
       	 	}
        }
        if(action.equals("okError") && popup.isVisible()) {
        	if (value) {
        		closePopup();
       	 	}
        }
	}
	
	public void returnToGame() {
		nifty.gotoScreen("gameOverLaye");
	}
	
	public void goToScreen(String screen) {
		nifty.gotoScreen(screen);
	}
	
	public void disconnect() {
		if(isServer) {
			server.disconnect();
			loaded = false;
			started = false;
			rootNode.detachAllChildren();
			LightList lightList = rootNode.getWorldLightList();
			for (int i = 0; i < lightList.size(); i++) {
				rootNode.removeLight(lightList.get(i));
			}
			world = null;
			nifty.gotoScreen("start");
		} else {

			if (client.isConnected()) {
				client.clientDisconnect();
			}
			String error = client.lastError;
			client = null;
			loaded = false;
			started = false;
			rootNode.detachAllChildren();
			LightList lightList = rootNode.getWorldLightList();
			for (int i = 0; i < lightList.size(); i++) {
				rootNode.removeLight(lightList.get(i));
			}
			nifty.gotoScreen("start");
			if (!error.equals("")) {
				showPopup("You were disconected from the server, " + error + ".");
			}

		}
	}
	
	public void showPopup(String s) {
		popup.findElementByName("panel").findElementByName("info").getRenderer(TextRenderer.class).setText(s);
		nifty.showPopup(nifty.getCurrentScreen(), popup.getId(), null);
		popupIsVisible = true;
	}
	
	public void closePopup() {
		if (popupIsVisible) {
			nifty.closePopup(popup.getId());
		}
		popupIsVisible = false;
		
	}
	
	public void quit() {
		System.exit(0);
	}
}
