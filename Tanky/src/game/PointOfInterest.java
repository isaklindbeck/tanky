package game;

import java.util.ArrayList;

import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.shape.Sphere;

public class PointOfInterest {
	protected Node mainNode;
	protected Node rootNode;
	protected AssetManager assetManager;
	protected Geometry geo;
	protected ArrayList<PointOfInterest> neighbouringPOI = new ArrayList<PointOfInterest>();
	
	public PointOfInterest(Vector3f pos, Node rootNode, AssetManager assetManager) {
		mainNode = new Node();
		mainNode.move(pos);
		this.rootNode = rootNode;
		this.assetManager = assetManager;
		
		Material mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
		mat.setFloat("m_Shininess", 5f);
		Sphere mesh = new Sphere(10,10,0.5f);
		
		geo = new Geometry("POI", mesh);
		geo.setMaterial(mat);
//		geo.setQueueBucket(Bucket.Transparent);
		geo.setCullHint(CullHint.Always);
		mainNode.attachChild(geo);
		
		rootNode.attachChild(mainNode);
	}
	
	public void lookForNeighbours(Node poiNode, ArrayList<PointOfInterest> otherPOI) {
		lookForNeighbour(poiNode, otherPOI, new Vector3f(1,0,0));
		lookForNeighbour(poiNode, otherPOI, new Vector3f(-1,0,0));
		lookForNeighbour(poiNode, otherPOI, new Vector3f(0,0,1));
		lookForNeighbour(poiNode, otherPOI, new Vector3f(0,0,-1));
//		System.out.println(neighbouringPOI.size());
	}
	
	public void lookForNeighbour(Node poiNode, ArrayList<PointOfInterest> otherPOI, Vector3f dir) {
		Ray ray = new Ray(mainNode.getWorldTranslation(), dir);
		
		CollisionResults results = new CollisionResults();
		poiNode.collideWith(ray, results);
		for (int i = 0; i < results.size(); i++) {
			if (results.getCollision(i).getGeometry().getParent() != mainNode) {
				for (int k = 0; k < otherPOI.size(); k++) {
					Geometry g = otherPOI.get(k).geo;
					if (results.getCollision(i).getGeometry() == g) {
						neighbouringPOI.add(otherPOI.get(k));
						k = otherPOI.size();
					} else if (results.getCollision(i).getGeometry().getName().equals("wall")) {
						k = otherPOI.size();
					}
				}
				i = results.size();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<PointOfInterest> getNeighbours() {
		return (ArrayList<PointOfInterest>) neighbouringPOI.clone();
	}
	
	public Node getMainNode() {
		return mainNode;
	}
}
