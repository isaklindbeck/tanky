package game;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

public class Wall extends GameObject {
	int width;

	public Wall(Vector3f pos, Node rootNode,	AssetManager assetManager, int width, boolean rotate) {
		super(pos, rootNode, assetManager);
		this.width = width;
		initWallModel();
		if (rotate){
			this.roateTankyObject();
		}
	}
	
	protected void initWallModel() {
        Material wall_mat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        wall_mat.setFloat("m_Shininess", 5f);
        Box wallBox = new Box(new Vector3f(0,2,0), width, 2f, 1.5f);
        Geometry wall = new Geometry("wall", wallBox);
        wall.setMaterial(wall_mat);
        mainNode.attachChild(wall);
        geo = wall;
	}
	
	@Override
	protected void initModel() {
		//Not used
	}
}
